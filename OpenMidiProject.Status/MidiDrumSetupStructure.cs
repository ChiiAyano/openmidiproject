﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.Status
{
	[StructLayout(LayoutKind.Sequential)]
	internal class MidiDrumSetupStructure
	{
		public IntPtr m_pParent;
		[MarshalAs(UnmanagedType.LPArray, SizeConst = (128))] public byte[] m_cDrumCutoffFrequency;
		[MarshalAs(UnmanagedType.LPArray, SizeConst = (128))] public byte[] m_cDrumResonance;
		[MarshalAs(UnmanagedType.LPArray, SizeConst = (128))] public byte[] m_cDrumAttackTime;
		[MarshalAs(UnmanagedType.LPArray, SizeConst = (128))] public byte[] m_cDrumDecay1Time;
		[MarshalAs(UnmanagedType.LPArray, SizeConst = (128))] public byte[] m_cDrumDecay2Time;
		[MarshalAs(UnmanagedType.LPArray, SizeConst = (128))] public byte[] m_cDrumPitchCoarse;
		[MarshalAs(UnmanagedType.LPArray, SizeConst = (128))] public byte[] m_cDrumPitchFine;
		[MarshalAs(UnmanagedType.LPArray, SizeConst = (128))] public byte[] m_cDrumVolume;
		[MarshalAs(UnmanagedType.LPArray, SizeConst = (128))] public byte[] m_cDrumPan;
		[MarshalAs(UnmanagedType.LPArray, SizeConst = (128))] public byte[] m_cDrumReverb;
		[MarshalAs(UnmanagedType.LPArray, SizeConst = (128))] public byte[] m_cDrumChorus;
		[MarshalAs(UnmanagedType.LPArray, SizeConst = (128))] public byte[] m_cDrumDelay;
		public int m_lUser1;
		public int m_lUser2;
		public int m_lUser3;
		public int m_lUserFlag;
	}
}
