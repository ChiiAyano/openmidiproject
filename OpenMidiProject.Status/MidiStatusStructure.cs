﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.Status
{
	[StructLayout(LayoutKind.Sequential)]
	public struct MidiStatusStructure
	{
		public int m_lModuleMode;
		public int m_lMasterFineTuning;
		public int m_lMasterCoarseTuning;
		public int m_lMasterBalance;
		public int m_lMasterVolume;
		public int m_lMasterPan;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)] public int[] m_lMasterReverb;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)] public int[] m_lMasterChorus;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)] public int[] m_lMasterDelay;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)] public int[] m_lMasterEqualizer;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)] public int[] m_lMasterInsertion;
		public int m_lNumMidiPart;
		public int m_lNumMidiDrumSetup;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
		public IntPtr[] m_pMidiPart;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
		public IntPtr[] m_pMidiDrumSetup;
		public int m_lUser1;
		public int m_lUser2;
		public int m_lUser3;
		public int m_lUserFlag;
		public int m_lRunningStatus;
	}
}
