﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.Status
{
	[StructLayout(LayoutKind.Sequential)]
	public struct MidiPartStructure
	{
		public IntPtr m_pParent;
		public int m_lPartMode;
		public int m_lOmniMonoPolyMode;
		public int m_lChannelFineTuning;
		public int m_lChannelCoarseTuning;
		public int m_PitchBendSensitivity;
		public int m_lModulationDepthRange;
		public int m_lReceiveChannel;
		public int m_lVelocitySenseDepth;
		public int m_lVelocitySenseOffset;
		public int m_lKeyboardRangeRow;
		public int m_lKeyboardRangeHigh;
		public int m_lAssignableControler1Num;
		public int m_lAssignableControler2Num;
		public int m_lReserved1;
		public int m_lReserved2;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)] public int[] m_lScaleOctaveTuning;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)] public byte[] m_cModulationDest;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)] public byte[] m_cPitchBendDest;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)] public byte[] m_cChannelAfterTouchDest;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)] public byte[] m_cAssignableControler1Dest;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)] public byte[] m_cAssignableControler2Dest;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)] public byte[] m_cNote;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)] public byte[] m_cNoteKeep;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)] public byte[] m_cKeyAfterTouch;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)] public byte[] m_ControlChange;
		public int m_lProgramChange;
		public int m_lChannelAfterTouch;
		public int m_lPitchBend;

		public int m_lUser1;
		public int m_lUser2;
		public int m_lUser3;
		public int m_lUserFlag;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = (128 * 128))] public int[] m_cNRPNMSB;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = (128 * 128))] public int[] m_cNRPNLSB;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = (128 * 128))] public int[] m_cRPNMSB;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = (128 * 128))] public int[] m_cRPNLSB;
	}
}
