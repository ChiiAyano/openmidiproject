﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.Status
{
	/// <summary>
	/// MIDI デバイスの状態を取得します。
	/// </summary>
    public class MidiStatus : IDisposable
	{
		#region MIDIStatus.dll 定義

		#region MidiPart

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_Delete")]
		private static extern int MIDIPart_Delete(IntPtr pMidiPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_Create")]
		private static extern IntPtr MIDIPart_Create(IntPtr pParent);

		// MIDIPart の Get 関数

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetPartMode")]
		private static extern int MIDIPart_GetPartMode(IntPtr pMidiPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetOmniMonoPolyMode")]
		private static extern int MIDIPart_GetOmniMonoPolyMode(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetChannelFineTuning")]
		private static extern int MIDIPart_GetChannelFineTuning(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetChannelCoarseTuning")]
		private static extern int MIDIPart_GetChannelCoarseTuning(IntPtr pMIDIPart);
	
		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetPitchBendSensitivity")]
		private static extern int MIDIPart_GetPitchBendSensitivity(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetModulationDepthRange")]
		private static extern int MIDIPart_GetModulationDepthRange(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetReceiveChannel")]
		private static extern int MIDIPart_GetReceiveChannel(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetVelocitySenseDepth")]
		private static extern int MIDIPart_GetVelocitySenseDepth(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetVelocitySenseOffset")]
		private static extern int MIDIPart_GetVelocitySenseOffset(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetKeyboardRangeLow")]
		private static extern int MIDIPart_GetKeyboardRangeLow(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetKeyboardRangeHigh")]
		private static extern int MIDIPart_GetKeyboardRangeHigh(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetNote")]
		private static extern int MIDIPart_GetNote(IntPtr pMIDIPart, int lKey);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetNoteEx")]
		private static extern int MIDIPart_GetNoteEx(IntPtr pMIDIPart, byte[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetNoteKeep")]
		private static extern int MIDIPart_GetNoteKeep(IntPtr pMIDIPart, int lKey);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetNoteKeepEx")]
		private static extern int MIDIPart_GetNoteKeepEx(IntPtr pMIDIPart, byte[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetKeyAfterTouch")]
		private static extern int MIDIPart_GetKeyAfterTouch(IntPtr pMIDIPart, int lKey);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetKeyAfterTouchEx")]
		private static extern int MIDIPart_GetKeyAfterTouchEx(IntPtr pMIDIPart, byte[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetControlChange")]
		private static extern int MIDIPart_GetControlChange(IntPtr pMIDIPart, int lKey);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetControlChangeEx")]
		private static extern int MIDIPart_GetControlChangeEx(IntPtr pMIDIPart, byte[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetRPNMSB")]
		private static extern int MIDIPart_GetRPNMSB(IntPtr pMIDIPart, int lCC101, int lCC100);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetRPNLSB")]
		private static extern int MIDIPart_GetRPNLSB(IntPtr pMIDIPart, int lCC101, int lCC100);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetNRPNMSB")]
		private static extern int MIDIPart_GetNRPNMSB(IntPtr pMIDIPart, int lCC101, int lCC100);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetNRPNLSB")]
		private static extern int MIDIPart_GetNRPNLSB(IntPtr pMIDIPart, int lCC101, int lCC100);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetProgramChange")]
		private static extern int MIDIPart_GetProgramChange(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetChannelAfterTouch")]
		private static extern int MIDIPart_GetChannelAfterTouch(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetPitchBend")]
		private static extern int MIDIPart_GetPitchBend(IntPtr pMIDIPart);

		// MIDIPart の Get 関数 (ユニーク)

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetNumNote")]
		private static extern int MIDIPart_GetNumNote(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetNumNoteKeep")]
		private static extern int MIDIPart_GetNumNoteKeep(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetHighestNote")]
		private static extern int MIDIPart_GetHighestNote(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetHighestNoteKeep")]
		private static extern int MIDIPart_GetHighestNoteKeep(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetLowestNote")]
		private static extern int MIDIPart_GetLowestNote(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetLowestNoteKeep")]
		private static extern int MIDIPart_GetLowestNoteKeep(IntPtr pMIDIPart);

		// MIDIPart の Set 関数

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_SetPartMode")]
		private static extern int MIDIPart_SetPartMode(IntPtr pMIDIPart, int lPartMode);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_SetOmniMonoPolyMode")]
		private static extern int MIDIPart_SetOmniMonoPolyMode(IntPtr pMIDIPart, int lOmuniMonoPolyMode);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetChannelFineTuning(IntPtr pMIDIPart, int lChannelFineTuning);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetChannelCoarseTuning(IntPtr pMIDIPart, int lChannelCoarseTuning);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetPitchBendSensitivity(IntPtr pMIDIPort, int lPitchBendSensitivity);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetModulationDepthRange(IntPtr pMIDIPort, int lModulationDepthRange);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetReseiveChannel(IntPtr pMIDIPort, int lReceiveChannel);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetVelocitySenseDepth(IntPtr pMIDIPort, int lVelocitySenseDepth);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetVelocitySenseOffset(IntPtr pMIDIPort, int lVelocitySenseOffset);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetKeyboardRangeLow(IntPtr pMIDIPort, int lKeyboardRangeLow);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetKeyboardRangeHigh(IntPtr pMIDIPort, int lKeyboardRangeHigh);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetNote(IntPtr pMIDIPort, int lKey, int lVel);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetKeyAfterTouch(IntPtr pMIDIPort, int lKey, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetControlChange(IntPtr pMIDIPort, int lNum, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetRPNMSB(IntPtr pMIDIPort, int lCC101, int lCC100, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetRPNLSB(IntPtr pMIDIPort, int lCC101, int lCC100, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetNRPNMSB(IntPtr pMIDIPort, int lCC99, int lCC98, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetNRPNLSB(IntPtr pMIDIPort, int lCC99, int lCC98, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetProgramChange(IntPtr pMIDIPort, int lNum);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetChannelAfterTouch(IntPtr pMIDIPort, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetPitchBend(IntPtr pMIDIPort, int lVal);

		#endregion

		#region MIDIDrumSetup

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_Delete(IntPtr pMIDIDrumSetup);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_Create(IntPtr pParent);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumCutoffFrequency(IntPtr pMIDIDrumSetup, int lKey);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumCutoffFrequencyEx(IntPtr pMIDIDrumSetup, ref byte pBuf, int lLen);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumResonance(IntPtr pMIDIDrumSetup, int lKey);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumResonanceEx(IntPtr pMIDIDrumSetup, byte[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumAttackTime(IntPtr pMIDIDrumSetup, int lKey);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumAttackTimeEx(IntPtr pMIDIDrumSetup, byte[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumDecay1Time(IntPtr pMIDIDrumSetup, int lKey);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumDecay1TimeEx(IntPtr pMIDIDrumSetup, byte[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumDecay2Time(IntPtr pMIDIDrumSetup, int lKey);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumDecay2TimeEx(IntPtr pMIDIDrumSetup, byte[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumPitchCoarse(IntPtr pMIDIDrumSetup, int lKey);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumPitchCoarseEx(IntPtr pMIDIDrumSetup, byte[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumPitchFine(IntPtr pMIDIDrumSetup, int lKey);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumPitchFineEx(IntPtr pMIDIDrumSetup, byte[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumVolume(IntPtr pMIDIDrumSetup, int lKey);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumVolumeEx(IntPtr pMIDIDrumSetup, byte[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumPan(IntPtr pMIDIDrumSetup, int lKey);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumPanEx(IntPtr pMIDIDrumSetup, byte[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumReverb(IntPtr pMIDIDrumSetup, int lKey);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumReverbEx(IntPtr pMIDIDrumSetup, byte[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumChorus(IntPtr pMIDIDrumSetup, int lKey);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumChorusEx(IntPtr pMIDIDrumSetup, byte[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumDelay(IntPtr pMIDIDrumSetup, int lKey);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_GetDrumDelayEx(IntPtr pMIDIDrumSetup, byte[] pBuf, int lLen);


		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_SetDrumCutoffFrequency(IntPtr pMIDIDrumSetup, int lKey,
		                                                               int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_SetDrumResonance(IntPtr pMIDIDrumSetup, int lKey, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_SetDrumAttackTime(IntPtr pMIDIDrumSetup, int lKey, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_SetDrumDecay1Time(IntPtr pMIDIDrumSetup, int lKey, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_SetDrumDecay2Time(IntPtr pMIDIDrumSetup, int lKey, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_SetDrumPitchCoarse(IntPtr pMIDIDrumSetup, int lKey, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_SetDrumPitchFine(IntPtr pMIDIDrumSetup, int lKey, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_SetDrumVolume(IntPtr pMIDIDrumSetup, int lKey, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_SetDrumPan(IntPtr pMIDIDrumSetup, int lKey, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_SetDrumReverb(IntPtr pMIDIDrumSetup, int lKey, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_SetDrumChorus(IntPtr pMIDIDrumSetup, int lKey, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIDrumSetup_SetDrumDelay(IntPtr pMIDIDrumSetup, int lKey, int lVal);

		#endregion

		#region MIDIStatus

		[DllImport("MIDIStatus.dll")]
		private static extern void MIDIStatus_Delete(IntPtr pMIDIStatus);

		[DllImport("MIDIStatus.dll")]
		private static extern IntPtr MIDIStatus_Create(int lModuleMode, int lNumMIDIPort, int lNumMIDIDrumSetup);

		// Master系は MasterStatus.cs にあります

		[DllImport("MIDIStatus.dll")]
		private static extern IntPtr MIDIStatus_GetMIDIPart(IntPtr pMIDIStatus, int lIndex);

		[DllImport("MIDIStatus.dll")]
		private static extern IntPtr MIDIStatus_GetMIDIDrumSetup(IntPtr pMIDIStatus, int lIndex);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_GetModuleMode(IntPtr pMIDIStatus);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_SetModuleMode(IntPtr pMIDIStatus, int lModuleMode);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_PutReset(IntPtr pMIDIStatus);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_PutGMReset(IntPtr pMIDIStatus);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_PutGM2Reset(IntPtr pMIDIStatus);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_PutGSReset(IntPtr pMIDIStatus);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_Put88Reset(IntPtr pMIDIStatus);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_PutXGReset(IntPtr pMIDIStatus);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_PutMIDIMessage(IntPtr pMIDIStatus, byte[] pMIDIMessage, int lLen);


		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_Save(IntPtr pMIDIStatus, string pszFileName);

		#endregion

		#endregion

		private IntPtr status;
		private MidiStatusStructure statusStructure;

		/// <summary>
		/// 現在のモジュール モードを取得または設定します。
		/// </summary>
		public ModuleMode ModuleMode
		{
			get
			{
				if (this.status == IntPtr.Zero) return 0;
				return (ModuleMode)MIDIStatus_GetModuleMode(this.status);
			}
			set
			{
				if (this.status == IntPtr.Zero) throw new InvalidOperationException("MIDI ステータスが生成されていないか無効です。");
				if (MIDIStatus_SetModuleMode(this.status, (int)value) == 0)
					throw new InvalidOperationException("モジュール モードの設定に失敗しました。");
			}
		}

		/// <summary>
		/// パート一覧を取得します。
		/// </summary>
		public IEnumerable<MidiPart> Part
		{
			get { if (this.status == IntPtr.Zero) return null;
				return Enumerable.Range(0, this.statusStructure.m_lNumMidiPart)
				                     .Select((s, i) => new MidiPart(MIDIStatus_GetMIDIPart(this.status, i)));
			}
		}


		private void Ctor(ModuleMode moduleMode, int part, int drums)
		{
			this.status = MIDIStatus_Create((int)moduleMode, part, drums);

			this.statusStructure = (MidiStatusStructure)Marshal.PtrToStructure(this.status, typeof(MidiStatusStructure));

			if (this.status == IntPtr.Zero)
				throw new InvalidOperationException("ステータス取得の準備に失敗しました。");
		}

		/// <summary>
		/// <see cref="OpenMidiProject.Status.MidiStatus"/> クラスの新しいインスタンスを生成します。
		/// </summary>
		/// <param name="moduleMode">ステータス取得の初期化モード</param>
		/// <param name="part">MIDI デバイスが使用できるパート数。通常は 16 パートです。</param>
		/// <param name="drums">MIDI デバイスが使用できるドラム パート数。GM/GM2/GS の場合は通常 2 パート、XG の場合は 4 パートです。</param>
		public MidiStatus(ModuleMode moduleMode, int part, int drums)
		{
			Ctor(moduleMode, part, drums);
		}

		public MidiStatus(ModuleMode moduleMode)
		{
			var drums = 0;

			switch (moduleMode)
			{
				case ModuleMode.Native:
				case ModuleMode.GM:
				case ModuleMode.GM2:
				case ModuleMode.GS:
				case ModuleMode.GS88:
					drums = 2;
					break;
				case ModuleMode.XG:
					drums = 4;
					break;
				default:
					drums = 2;
					break;
			}

			Ctor(moduleMode, 16, drums);
		}

		/// <summary>
		/// MIDI ステータスへ MIDI メッセージを送信します。
		/// </summary>
		/// <param name="message"></param>
		public void SendMessage(byte[] message)
		{
			if(this.status==IntPtr.Zero)
				throw new InvalidOperationException("MIDI ステータスが生成されていないか無効です。");

			if (MIDIStatus_PutMIDIMessage(this.status, message, message.Length) == 0)
				throw new InvalidOperationException("MIDI メッセージを解析できませんでした。");
		}


		public void Close()
		{
			if (this.status != IntPtr.Zero)
				MIDIStatus_Delete(this.status);
		}

		public void Dispose()
		{
			Close();
		}
	}
}
