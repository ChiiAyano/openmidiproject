﻿namespace OpenMidiProject.Status
{
	public enum OmniMonoPolyMode
	{
		OmniOnPoly = 1,
		OmniOnMono = 2,
		OmniOffPoly = 3,
		OmniOffMono = 4
	}
}