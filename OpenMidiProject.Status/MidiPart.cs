﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.Status
{
	/// <summary>
	/// 各パートの設定。
	/// </summary>
	public class MidiPart
	{
		#region API 定義

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_Delete")]
		private static extern int MIDIPart_Delete(IntPtr pMidiPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_Create")]
		private static extern IntPtr MIDIPart_Create(IntPtr pParent);

		// MIDIPart の Get 関数

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetPartMode")]
		private static extern int MIDIPart_GetPartMode(IntPtr pMidiPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetOmniMonoPolyMode")]
		private static extern int MIDIPart_GetOmniMonoPolyMode(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetChannelFineTuning")]
		private static extern int MIDIPart_GetChannelFineTuning(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetChannelCoarseTuning")]
		private static extern int MIDIPart_GetChannelCoarseTuning(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetPitchBendSensitivity")]
		private static extern int MIDIPart_GetPitchBendSensitivity(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetModulationDepthRange")]
		private static extern int MIDIPart_GetModulationDepthRange(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetReceiveChannel")]
		private static extern int MIDIPart_GetReceiveChannel(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetVelocitySenseDepth")]
		private static extern int MIDIPart_GetVelocitySenseDepth(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetVelocitySenseOffset")]
		private static extern int MIDIPart_GetVelocitySenseOffset(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetKeyboardRangeLow")]
		private static extern int MIDIPart_GetKeyboardRangeLow(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetKeyboardRangeHigh")]
		private static extern int MIDIPart_GetKeyboardRangeHigh(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetNote")]
		private static extern int MIDIPart_GetNote(IntPtr pMIDIPart, int lKey);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetNoteEx")]
		private static extern int MIDIPart_GetNoteEx(IntPtr pMIDIPart, byte[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetNoteKeep")]
		private static extern int MIDIPart_GetNoteKeep(IntPtr pMIDIPart, int lKey);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetNoteKeepEx")]
		private static extern int MIDIPart_GetNoteKeepEx(IntPtr pMIDIPart, byte[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetKeyAfterTouch")]
		private static extern int MIDIPart_GetKeyAfterTouch(IntPtr pMIDIPart, int lKey);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetKeyAfterTouchEx")]
		private static extern int MIDIPart_GetKeyAfterTouchEx(IntPtr pMIDIPart, byte[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetControlChange")]
		private static extern int MIDIPart_GetControlChange(IntPtr pMIDIPart, int lKey);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetControlChangeEx")]
		private static extern int MIDIPart_GetControlChangeEx(IntPtr pMIDIPart, byte[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetRPNMSB")]
		private static extern int MIDIPart_GetRPNMSB(IntPtr pMIDIPart, int lCC101, int lCC100);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetRPNLSB")]
		private static extern int MIDIPart_GetRPNLSB(IntPtr pMIDIPart, int lCC101, int lCC100);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetNRPNMSB")]
		private static extern int MIDIPart_GetNRPNMSB(IntPtr pMIDIPart, int lCC101, int lCC100);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetNRPNLSB")]
		private static extern int MIDIPart_GetNRPNLSB(IntPtr pMIDIPart, int lCC101, int lCC100);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetProgramChange")]
		private static extern int MIDIPart_GetProgramChange(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetChannelAfterTouch")]
		private static extern int MIDIPart_GetChannelAfterTouch(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetPitchBend")]
		private static extern int MIDIPart_GetPitchBend(IntPtr pMIDIPart);

		// MIDIPart の Get 関数 (ユニーク)

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetNumNote")]
		private static extern int MIDIPart_GetNumNote(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetNumNoteKeep")]
		private static extern int MIDIPart_GetNumNoteKeep(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetHighestNote")]
		private static extern int MIDIPart_GetHighestNote(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetHighestNoteKeep")]
		private static extern int MIDIPart_GetHighestNoteKeep(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetLowestNote")]
		private static extern int MIDIPart_GetLowestNote(IntPtr pMIDIPart);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_GetLowestNoteKeep")]
		private static extern int MIDIPart_GetLowestNoteKeep(IntPtr pMIDIPart);

		// MIDIPart の Set 関数

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_SetPartMode")]
		private static extern int MIDIPart_SetPartMode(IntPtr pMIDIPart, int lPartMode);

		[DllImport("MIDIStatus.dll", EntryPoint = "MIDIPart_SetOmniMonoPolyMode")]
		private static extern int MIDIPart_SetOmniMonoPolyMode(IntPtr pMIDIPart, int lOmuniMonoPolyMode);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetChannelFineTuning(IntPtr pMIDIPart, int lChannelFineTuning);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetChannelCoarseTuning(IntPtr pMIDIPart, int lChannelCoarseTuning);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetPitchBendSensitivity(IntPtr pMIDIPort, int lPitchBendSensitivity);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetModulationDepthRange(IntPtr pMIDIPort, int lModulationDepthRange);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetReseiveChannel(IntPtr pMIDIPort, int lReceiveChannel);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetVelocitySenseDepth(IntPtr pMIDIPort, int lVelocitySenseDepth);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetVelocitySenseOffset(IntPtr pMIDIPort, int lVelocitySenseOffset);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetKeyboardRangeLow(IntPtr pMIDIPort, int lKeyboardRangeLow);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetKeyboardRangeHigh(IntPtr pMIDIPort, int lKeyboardRangeHigh);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetNote(IntPtr pMIDIPort, int lKey, int lVel);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetKeyAfterTouch(IntPtr pMIDIPort, int lKey, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetControlChange(IntPtr pMIDIPort, int lNum, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetRPNMSB(IntPtr pMIDIPort, int lCC101, int lCC100, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetRPNLSB(IntPtr pMIDIPort, int lCC101, int lCC100, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetNRPNMSB(IntPtr pMIDIPort, int lCC99, int lCC98, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetNRPNLSB(IntPtr pMIDIPort, int lCC99, int lCC98, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetProgramChange(IntPtr pMIDIPort, int lNum);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetChannelAfterTouch(IntPtr pMIDIPort, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIPart_SetPitchBend(IntPtr pMIDIPort, int lVal);

		#endregion


		private IntPtr midiPart = IntPtr.Zero;

		internal MidiPart(IntPtr midiPart)
		{
			this.midiPart = midiPart;
		}

		/// <summary>
		/// このパートのモードを取得または設定します。
		/// </summary>
		public PartMode PartMode
		{
			get
			{
				if (this.midiPart == IntPtr.Zero) return 0;

				return (PartMode) MIDIPart_GetPartMode(this.midiPart);
			}
			set
			{
				if (this.midiPart == IntPtr.Zero) throw new InvalidOperationException("MIDI パートが生成されていないか無効です。");
				if (MIDIPart_SetPartMode(this.midiPart, (int) value) == 0)
					throw new InvalidOperationException("パートの設定に失敗しました。");
			}
		}

		/// <summary>
		/// このパートのオムニ モードと、モノ/ポリ モードの状態を取得または設定します。
		/// </summary>
		public OmniMonoPolyMode OmniMonoPolyMode
		{
			get
			{
				if (this.midiPart == IntPtr.Zero) return 0;

				return (OmniMonoPolyMode) MIDIPart_GetOmniMonoPolyMode(this.midiPart);
			}
			set
			{
				if (this.midiPart == IntPtr.Zero) throw new InvalidOperationException("MIDI パートが生成されていないか無効です。");
				if (MIDIPart_SetOmniMonoPolyMode(this.midiPart, (int) value) == 0)
					throw new InvalidOperationException("オムニモード、モノ/ポリの設定に失敗しました。");
			}
		}

		/// <summary>
		/// このパートのファイン チューニングの値を取得または設定します。
		/// </summary>
		public int FineTuning
		{
			get
			{
				if (this.midiPart == IntPtr.Zero) return 0;

				return MIDIPart_GetChannelFineTuning(this.midiPart);
			}
			set
			{
				if (this.midiPart == IntPtr.Zero) throw new InvalidOperationException("MIDI パートが生成されていないか無効です。");
				if (MIDIPart_SetChannelFineTuning(this.midiPart, value) == 0)
					throw new InvalidOperationException("ファイン チューニングの設定に失敗しました。");
			}
		}

		/// <summary>
		/// このパートのキー シフトの値を取得または設定します。
		/// </summary>
		public int CoarseTuning
		{
			get
			{
				if (this.midiPart == IntPtr.Zero) return 0;

				return MIDIPart_GetChannelCoarseTuning(this.midiPart);
			}
			set
			{
				if (this.midiPart == IntPtr.Zero) throw new InvalidOperationException("MIDI パートが生成されていないか無効です。");
				if (MIDIPart_SetChannelCoarseTuning(this.midiPart, value) == 0)
					throw new InvalidOperationException("キー シフトの設定に失敗しました。");
			}
		}

		/// <summary>
		/// このパートのピッチ ベンド幅の値を取得または設定します。
		/// </summary>
		public int PitchBendSensitivity
		{
			get
			{
				if (this.midiPart == IntPtr.Zero) return 0;

				return MIDIPart_GetPitchBendSensitivity(this.midiPart);
			}
			set
			{
				if (this.midiPart == IntPtr.Zero) throw new InvalidOperationException("MIDI パートが生成されていないか無効です。");
				if (MIDIPart_SetPitchBend(this.midiPart, value) == 0)
					throw new InvalidOperationException("ピッチ ベンド幅の設定に失敗しました。");
			}
		}

		/// <summary>
		/// このパートのモジュレーションの変化幅の値を取得または設定します。
		/// </summary>
		public int ModulationDepthRange
		{
			get
			{
				if (this.midiPart == IntPtr.Zero) return 0;

				return MIDIPart_GetModulationDepthRange(this.midiPart);
			}
			set
			{
				if (this.midiPart == IntPtr.Zero) throw new InvalidOperationException("MIDI パートが生成されていないか無効です。");
				if (MIDIPart_SetModulationDepthRange(this.midiPart, value) == 0)
					throw new InvalidOperationException("モジュレーションの変化幅の設定に失敗しました。");
			}
		}

		/// <summary>
		/// このパートの受信チャンネルを取得または設定します。
		/// </summary>
		/// <remarks>パートとチャンネルは対一である必要はなく、たとえば Part 1 と Part 2 を同一チャンネルにすれば、Part 1 で再生した情報がそのまま Part 2 にも流れるようになります。</remarks>
		public int ReceiveChannel
		{
			get
			{
				if (this.midiPart == IntPtr.Zero) return 0;

				return MIDIPart_GetReceiveChannel(this.midiPart);
			}
			set
			{
				if (this.midiPart == IntPtr.Zero) throw new InvalidOperationException("MIDI パートが生成されていないか無効です。");
				if (MIDIPart_SetReseiveChannel(this.midiPart, value) == 0)
					throw new InvalidOperationException("受信チャンネルの設定に失敗しました。");
			}
		}

		/// <summary>
		/// このパートのベロシティ センス デプスの値を取得または設定します。
		/// </summary>
		public int VelocitySenseDepth
		{
			get
			{
				if (this.midiPart == IntPtr.Zero) return 0;

				return MIDIPart_GetVelocitySenseDepth(this.midiPart);
			}
			set
			{
				if (this.midiPart == IntPtr.Zero) throw new InvalidOperationException("MIDI パートが生成されていないか無効です。");
				if (MIDIPart_SetVelocitySenseDepth(this.midiPart, value) == 0)
					throw new InvalidOperationException("ベロシティ センス デプスの設定に失敗しました。");
			}
		}

		/// <summary>
		/// このパートのベロシティ センス オフセットの変化幅の値を取得または設定します。
		/// </summary>
		public int VelocitySenseOffset
		{
			get
			{
				if (this.midiPart == IntPtr.Zero) return 0;

				return MIDIPart_GetVelocitySenseOffset(this.midiPart);
			}
			set
			{
				if (this.midiPart == IntPtr.Zero) throw new InvalidOperationException("MIDI パートが生成されていないか無効です。");
				if (MIDIPart_SetVelocitySenseOffset(this.midiPart, value) == 0)
					throw new InvalidOperationException("ベロシティ センス オフセットの設定に失敗しました。");
			}
		}

		/// <summary>
		/// このパートのキーボード下限値を取得または設定します。
		/// </summary>
		public int KeyboardRangeLow
		{
			get
			{
				if (this.midiPart == IntPtr.Zero) return 0;

				return MIDIPart_GetKeyboardRangeLow(this.midiPart);
			}
			set
			{
				if (this.midiPart == IntPtr.Zero) throw new InvalidOperationException("MIDI パートが生成されていないか無効です。");
				if (MIDIPart_SetKeyboardRangeLow(this.midiPart, value) == 0)
					throw new InvalidOperationException("キーボード下限値の設定に失敗しました。");
			}
		}

		/// <summary>
		/// このパートのキーボード上限値を取得または設定します。
		/// </summary>
		public int KeyboardRangeHigh
		{
			get
			{
				if (this.midiPart == IntPtr.Zero) return 0;

				return MIDIPart_GetKeyboardRangeHigh(this.midiPart);
			}
			set
			{
				if (this.midiPart == IntPtr.Zero) throw new InvalidOperationException("MIDI パートが生成されていないか無効です。");
				if (MIDIPart_SetKeyboardRangeHigh(this.midiPart, value) == 0)
					throw new InvalidOperationException("キーボード上限値の設定に失敗しました。");
			}
		}

		/// <summary>
		/// このパートでの現在のノート情報を取得します。
		/// </summary>
		/// <remarks>要素番号は音階を表し、その値は 0 の場合ノート オフ、それ以上の場合はベロシティを表します。</remarks>
		public byte[] Notes
		{
			get
			{
				if (this.midiPart == IntPtr.Zero) return null;

				var notes = new byte[128];
				MIDIPart_GetNoteEx(this.midiPart, notes, notes.Length);

				return notes;
			}
		}

		/// <summary>
		/// このパートでの現在のノート情報を取得します。
		/// </summary>
		/// <remarks>要素番号は音階を表し、その値は 0 の場合ノート オフ、それ以上の場合はベロシティを表します。</remarks>
		public byte[] NotesKeep
		{
			get
			{
				if (this.midiPart == IntPtr.Zero) return null;

				var notes = new byte[128];
				MIDIPart_GetNoteKeepEx(this.midiPart, notes, notes.Length);

				return notes;
			}
		}

		/// <summary>
		/// このパートでの現在のキー アフター タッチ情報を取得します。
		/// </summary>
		public byte[] KeyAfterTouch
		{
			get
			{
				if (this.midiPart == IntPtr.Zero) return null;

				var notes = new byte[128];
				MIDIPart_GetKeyAfterTouchEx(this.midiPart, notes, notes.Length);

				return notes;
			}
		}

		/// <summary>
		/// このパートでの現在のコントロール チェンジの情報を取得します。
		/// </summary>
		public byte[] ControlChange
		{
			get
			{
				if (this.midiPart == IntPtr.Zero) return null;

				var cc = new byte[128];
				MIDIPart_GetControlChangeEx(this.midiPart, cc, cc.Length);

				return cc;
			}
		}

		/// <summary>
		/// このパートのプログラム チェンジ (音色) の値を取得または設定します。
		/// </summary>
		public int ProgramChange
		{
			get
			{
				if (this.midiPart == IntPtr.Zero) return 0;

				return MIDIPart_GetProgramChange(this.midiPart);
			}
			set
			{
				if (this.midiPart == IntPtr.Zero) throw new InvalidOperationException("MIDI パートが生成されていないか無効です。");
				if (MIDIPart_SetProgramChange(this.midiPart, value) == 0)
					throw new InvalidOperationException("プログラム チェンジの設定に失敗しました。");
			}
		}

		/// <summary>
		/// このパートのピッチ ベンドの値を取得または設定します。
		/// </summary>
		public int PitchBend
		{
			get
			{
				if (this.midiPart == IntPtr.Zero) return 0;

				return MIDIPart_GetPitchBend(this.midiPart);
			}
			set
			{
				if (this.midiPart == IntPtr.Zero) throw new InvalidOperationException("MIDI パートが生成されていないか無効です。");
				if (MIDIPart_SetPitchBend(this.midiPart, value) == 0)
					throw new InvalidOperationException("ピッチ ベンドの設定に失敗しました。");
			}
		}


		public MidiPartStructure ToStructure()
		{
			return (MidiPartStructure)Marshal.PtrToStructure(this.midiPart, typeof(MidiPartStructure));
		}
	}
}