﻿namespace OpenMidiProject.Status
{
	/// <summary>
	/// パートのモードを定義します。
	/// </summary>
	public enum PartMode
	{
		/// <summary>
		/// このパートは音色楽器です。
		/// </summary>
		Instrument = 0,
		/// <summary>
		/// このパートはドラム (ドラム 1) です。
		/// </summary>
		Drum1 = 1,
		/// <summary>
		/// このパートはドラム (ドラム 2) です。
		/// </summary>
		Drum2 = 2,
		/// <summary>
		/// このパートはドラム (ドラム 3) です。
		/// </summary>
		Drum3 = 3,
		/// <summary>
		/// このパートはドラム (ドラム 4) です。
		/// </summary>
		Drum4 = 4
	}
}