﻿namespace OpenMidiProject.Status
{
	public enum ModuleMode
	{
		Native =	0x00000000,
		GM =		0x7e000001,
		GM2 =		0x7e000003,
		GS =		0x41000002,
		GS88 =		0x41000003,
		XG =		0x43000002
	}
}