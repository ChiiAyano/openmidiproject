﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.Status
{
	/// <summary>
	/// MIDI デバイス全体が持つステータス。
	/// </summary>
	public class MasterStatus
	{
		#region API 定義

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_GetMasterFineTuning(IntPtr pMIDIStatus);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_GetMasterCoarseTuning(IntPtr pMidiStatus);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_GetMasterVolume(IntPtr pMIDIStatus);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_GetMasterPan(IntPtr pMIDIStatus);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_GetMasterReverb(IntPtr pMIDIStatus, int lNum);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_GetMasterReverbEx(IntPtr pMIDIStatus, int[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_GetMasterChorus(IntPtr pMIDIStatus, int lNum);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_GetMasterChorusEx(IntPtr pMIDIStatus, int[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_GetMasterDelay(IntPtr pMIDIStatus, int lNum);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_GetMasterDelayEx(IntPtr pMIDIStatus, int[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_GetMasterEqualizer(IntPtr pMIDIStatus, int lNum);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_GetMasterEqualizerEx(IntPtr pMIDIStatus, int[] pBuf, int lLen);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_GetNumMIDIPart(IntPtr pMIDIStatus);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_GetNumMIDIDrumSetup(IntPtr pMIDIStatus);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_SetMasterFineTuning(IntPtr pMIDIStatus, int lMasterFineTuning);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_SetMasterCoarseTuning(IntPtr pMIDIStatus, int lMasterCoarseTuning);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_SetMasterVolume(IntPtr pMIDIStatus, int lMasterVolume);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_SetMasterPan(IntPtr pMIDIStatus, int lMasterPan);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_SetMasterReverb(IntPtr pMIDIStatus, int lNum, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern void MIDIStatus_SetMasterReverbType(IntPtr pMIDIStatus, int lMasterReverbType);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_SetMasterChorus(IntPtr pMIDIStatus, int lNum, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern void MIDIStatus_SetMasterChorusType(IntPtr pMIDIStatus, int lMasterChorusType);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_SetMasterDelay(IntPtr pMIDIStatus, int lNum, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern void MIDIStatus_SetMasterDelayType(IntPtr pMIDIStatus, int lMasterDelayType);

		[DllImport("MIDIStatus.dll")]
		private static extern int MIDIStatus_SetMasterEqualizer(IntPtr pMIDIStatus, int lNum, int lVal);

		[DllImport("MIDIStatus.dll")]
		private static extern void MIDIStatus_SetMasterEqualizerType(IntPtr pMIDIStatus, int lMasterEqualizerType);

		#endregion

		private IntPtr midiStatus = IntPtr.Zero;

		internal MasterStatus(IntPtr midiStatus)
		{
			this.midiStatus = midiStatus;
		}

		/// <summary>
		/// マスター ファイン チューニングの値を取得または設定します。
		/// </summary>
		public int MasterFineTuning
		{
			get
			{
				if (this.midiStatus == IntPtr.Zero) return 0;

				return MIDIStatus_GetMasterFineTuning(this.midiStatus);
			}
			set
			{
				if (this.midiStatus == IntPtr.Zero) throw new InvalidOperationException("MIDI ステータスが生成されていないか無効です。");
				if (MIDIStatus_SetMasterFineTuning(this.midiStatus, value) == 0)
					throw new InvalidOperationException("ファイン チューニングの設定に失敗しました。");
			}
		}

		/// <summary>
		/// マスター キーシフトの値を取得または設定します。
		/// </summary>
		public int MasterCoarseTuning
		{
			get
			{
				if (this.midiStatus == IntPtr.Zero) return 0;

				return MIDIStatus_GetMasterCoarseTuning(this.midiStatus);
			}
			set
			{
				if (this.midiStatus == IntPtr.Zero) throw new InvalidOperationException("MIDI ステータスが生成されていないか無効です。");
				if (MIDIStatus_SetMasterCoarseTuning(this.midiStatus, value) == 0)
					throw new InvalidOperationException("キーシフトの設定に失敗しました。");
			}
		}

		/// <summary>
		/// マスター ボリュームの値を取得または設定します。
		/// </summary>
		public int MasterVolume
		{
			get
			{
				if (this.midiStatus == IntPtr.Zero) return 0;

				return MIDIStatus_GetMasterVolume(this.midiStatus);
			}
			set
			{
				if (this.midiStatus == IntPtr.Zero) throw new InvalidOperationException("MIDI ステータスが生成されていないか無効です。");
				if (MIDIStatus_SetMasterVolume(this.midiStatus, value) == 0)
					throw new InvalidOperationException("ボリュームの設定に失敗しました。");
			}
		}

		/// <summary>
		/// マスター パン (定位) の値を取得または設定します。
		/// </summary>
		public int MasterPan
		{
			get
			{
				if (this.midiStatus == IntPtr.Zero) return 0;

				return MIDIStatus_GetMasterPan(this.midiStatus);
			}
			set
			{
				if (this.midiStatus == IntPtr.Zero) throw new InvalidOperationException("MIDI ステータスが生成されていないか無効です。");
				if (MIDIStatus_SetMasterPan(this.midiStatus, value) == 0)
					throw new InvalidOperationException("パンの設定に失敗しました。");
			}
		}

		/// <summary>
		/// マスター リバーブの値を取得または設定します。
		/// </summary>
		public IEnumerable<int> MasterReverb
		{
			get
			{
				if (this.midiStatus == IntPtr.Zero) return null;

				var reverb = new int[32];

				MIDIStatus_GetMasterReverbEx(this.midiStatus, reverb, reverb.Length);

				return reverb;
			}
			set
			{
				if (this.midiStatus == IntPtr.Zero) throw new InvalidOperationException("MIDI ステータスが生成されていないか無効です。");

				var reverb = value.ToArray();
				for (int i = 0; i < reverb.Length; i++)
				{
					if (MIDIStatus_SetMasterReverb(this.midiStatus, i, reverb[i]) == 0)
						throw new InvalidOperationException("リバーブの設定に失敗しました。");
				}
			}
		}

		/// <summary>
		/// マスター コーラスの値を取得または設定します。
		/// </summary>
		public IEnumerable<int> MasterChorus
		{
			get
			{
				if (this.midiStatus == IntPtr.Zero) return null;

				var chorus = new int[32];

				MIDIStatus_GetMasterChorusEx(this.midiStatus, chorus, chorus.Length);

				return chorus;
			}
			set
			{
				if (this.midiStatus == IntPtr.Zero) throw new InvalidOperationException("MIDI ステータスが生成されていないか無効です。");

				var chorus = value.ToArray();
				for (int i = 0; i < chorus.Length; i++)
				{
					if (MIDIStatus_SetMasterChorus(this.midiStatus, i, chorus[i]) == 0)
						throw new InvalidOperationException("コーラスの設定に失敗しました。");
				}
			}
		}

		/// <summary>
		/// マスター ディレイの値を取得または設定します。
		/// </summary>
		public IEnumerable<int> MasterDelay
		{
			get
			{
				if (this.midiStatus == IntPtr.Zero) return null;

				var delay = new int[32];

				MIDIStatus_GetMasterDelayEx(this.midiStatus, delay, delay.Length);

				return delay;
			}
			set
			{
				if (this.midiStatus == IntPtr.Zero) throw new InvalidOperationException("MIDI ステータスが生成されていないか無効です。");

				var delay = value.ToArray();
				for (int i = 0; i < delay.Length; i++)
				{
					if (MIDIStatus_SetMasterDelay(this.midiStatus, i, delay[i]) == 0)
						throw new InvalidOperationException("ディレイの設定に失敗しました。");
				}
			}
		}

		/// <summary>
		/// マスター イコライザーの値を取得または設定します。
		/// </summary>
		public IEnumerable<int> MasterEqualizer
		{
			get
			{
				if (this.midiStatus == IntPtr.Zero) return null;

				var equalizer = new int[32];

				MIDIStatus_GetMasterEqualizerEx(this.midiStatus, equalizer, equalizer.Length);

				return equalizer;
			}
			set
			{
				if (this.midiStatus == IntPtr.Zero) throw new InvalidOperationException("MIDI ステータスが生成されていないか無効です。");

				var equalizer = value.ToArray();
				for (int i = 0; i < equalizer.Length; i++)
				{
					if (MIDIStatus_SetMasterEqualizer(this.midiStatus, i, equalizer[i]) == 0)
						throw new InvalidOperationException("イコライザーの設定に失敗しました。");
				}
			}
		}
	}
}
