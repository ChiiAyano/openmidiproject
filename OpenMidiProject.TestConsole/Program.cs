﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenMidiProject.IO;
using OpenMidiProject.Clock;
using System.Threading;
using OpenMidiProject.Status;
using OpenMidiProject.Midi;
using OpenMidiProject.Data;

namespace OpenMidiProject.TestConsole
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				Console.WriteLine("MIDI Out Device Count: " + MidiOut.DeviceCount);
				Console.WriteLine("Devices:");
				Console.WriteLine(string.Join("\n", MidiOut.DeviceNames.Select((s, i) => (i) + ". " + s)));

				Console.WriteLine("Device In Count: " + MidiIn.DeviceCount);
				Console.WriteLine("Devices:");
				Console.WriteLine(string.Join("\n", MidiIn.DeviceNames.Select((s, i) => (i) + ". " + s)));


				var number = 0;

				while (true)
				{
					Console.Write("使用する MIDI Out デバイス番号: ");
					var read = Console.ReadLine();
					if (int.TryParse(read, out number))
						break;
				}

				using (var midi = new MidiManager(new[] { MidiOut.Create(number) }) { Tempo = 120, Speed = 1 })
				{

					// セットアップトラックの設定
					//var setup = midi.Tracks.FirstOrDefault(f => f.IsSetupTrack);
					//setup.AddEvent(MidiEvent.CreateMeter(1920, 4, 4, midi.Timebase));

					//var track = MidiTrack.CreateNew();
					//track.OutputChannel = 0;
					//track.AddRangeEvent(new[]
					//						{
					//							MidiEvent.CreateProgramChange(10, 0, 0),
					//							MidiEvent.CreateNoteOnNoteOff(1920, 0, 60, 127, 0, 480),
					//							MidiEvent.CreateNoteOnNoteOff(1920 + 480, 0, 62, 127, 0, 480),
					//							MidiEvent.CreateNoteOnNoteOff(1920 + (480 * 2), 0, 64, 127, 0, 480),
					//							MidiEvent.CreateNoteOnNoteOff(1920 + (480 * 3), 0, 65, 127, 0, 480)
					//						});
					//midi.Tracks.Add(track);

					midi.LoadFile(@"C:\Windows\Media\onestop.mid");

					midi.Tick += (s, e) => Console.Title = string.Format("{0} / Tempo: {1:0.000}", midi.Title, midi.Tempo);

					//midi.Tick +=
					//	(s, e) =>
					//	Console.WriteLine("{0:00.000} sec> {1:00}:{2:00}:{3:000} ({4})", e.ElapsedTime.TotalSeconds, e.Measure, e.Beat,
					//					  e.Tick, e.ElapsedTick);
					midi.Play();

					Console.ReadKey();
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				Console.WriteLine(ex.StackTrace);
			}

		}

	}
}
