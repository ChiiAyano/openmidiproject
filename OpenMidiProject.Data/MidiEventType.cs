﻿namespace OpenMidiProject.Data
{
	public enum MidiEventType
	{
		SequenceNumber = 0x00,
		TextEvent = 0x01,
		CopyrightNotice = 0x02,
		TrackName = 0x03,
		InstrumentName = 0x04,
		Lyric = 0x05,
		Marker = 0x06,
		CuePoint = 0x07,
		ProgramName = 0x08,
		DeviceName = 0x09,
		ChannelPrefix = 0x20,
		PortPrefix = 0x21,
		EndofTrack = 0x2f,
		Tempo = 0x51,
		SmpteOffSet = 0x54,
		TimeSignature = 0x58,
		KeySignature = 0x59,
		SequencerPrecific = 0x7f,
		NoteOff = 0x80,
		NoteOn = 0x90,
		KeyAfterTouch = 0xa0,
		ControlChange = 0xb0,
		ProgramChange = 0xc0,
		ChannelAfterTouch = 0xd0,
		PitchBend = 0xe0,
		SysExStart = 0xf0,
		SysExContinue = 0xf7,

		Unknown = 0xff
	}
}