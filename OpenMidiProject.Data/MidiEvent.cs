﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.Data
{
	/// <summary>
	/// 
	/// </summary>
	public class MidiEvent : IDisposable
	{
		#region API 定義

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_GetFirstCombinedEvent(IntPtr pEvent);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_GetLastCombinedEvent(IntPtr pEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_Combine(IntPtr pEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_Chop(IntPtr pEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_DeleteSingle(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_Delete(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_Create(int lTime, int lKind, byte[] pData, int lLen);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateClone(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateSequenceNumber(int lTime, int lNum);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIEvent_CreateTextEventW(int lTime, string pszText);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIEvent_CreateCopyrightNoticeW(int lTime, string pszText);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIEvent_CreateTrackNameW(int lTime, string pszText);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIEvent_CreateInstrumentNameW(int lTime, string pszText);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIEvent_CreateLyricW(int lTime, string pszText);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIEvent_CreateMakerW(int lTime, string pszText);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIEvent_CreateCuePointW(int lTime, string pszText);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIEvent_CreateProgramNameW(int lTime, string pszText);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIEvent_CreateDeviceNameW(int lTime, string pszText);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateChannelPrefix(int lTime, int lCh);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreatePortPrefix(int lTime, int lNum);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateEndofTrack(int lTime);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateTempo(int lTime, int lTempo);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateSMPTEOffset(int lTime, int lMode, int lHour, int lMin, int lSec,
		                                                         int lFrame, int lSubFrame);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateTimeSignature(int lTime, int lnn, int ldd, int lcc, int lbb);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateKeySignature(int lTime, int lsf, int lmi);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateSequencerSpecific(int lTime, char[] pData, int lLen);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateNoteOff(int lTime, int lCh, int lKey, int lVel);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateNoteOn(int lTime, int lCh, int lKey, int lVel);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateNote(int lTime, int lCh, int lKey, int lVel, int lDur);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateNoteOnNoteOff(int lTime, int lCh, int lKey, int lVel1, int lVel2,
		                                                           int lDur);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateNoteOnNoteOn0(int lTime, int lCh, int lKey, int lVel, int lDur);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateKeyAftertouch(int lTime, int lCh, int lKey, int lVal);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateControlChange(int lTime, int lCh, int lNum, int lVal);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateRPNChange(int lTime, int lCh, int lBank, int lVal);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateNRPNChange(int lTime, int lCh, int lBank, int lVal);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateProgramChange(int lTime, int lCh, int lNum);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreatePatchChange(int lTime, int lCh, int lBank, int lVal);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateChannelAftertouch(int lTime, int lCh, int lVal);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreatePitchBend(int lTime, int lCh, int lVal);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_CreateSysEx(int time, byte[] pBuf, int lLen);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsMetaEvent(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsSequenceNumber(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsTextEvent(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsCopyrightNotice(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsTrackName(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsInstrumentName(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsLyric(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsMarker(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsCuePoint(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsProgramName(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsDeviceName(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsChannelPrefix(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsPortPrefix(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsEndofTrack(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsTempo(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsSMPTEOffset(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsTimeSignature(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsKeySignature(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsSequencerSpecific(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsMIDIEvent(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsNoteOn(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsNoteOff(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsNote(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsNoteOnNoteOff(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsNoteOnNoteOn0(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsKeyAftertouch(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsControlChange(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsRPNChange(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsNRPNChange(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsProgramChange(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsPatchChange(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsChannelAftertouch(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsPitchBend(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsSysExEvent(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsFloating(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_IsCombined(IntPtr pMidiEvent);


		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_GetKind(IntPtr pEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_SetKind(IntPtr pEvent, int lKind);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_GetLen(IntPtr pEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_GetData(IntPtr pEvent, byte[] pBuf, int lLen);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_SetData(IntPtr pEvent, byte[] pBuf, int lLen);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIEvent_GetTextW(IntPtr pEvent, char[] pBuf, int lLen);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDIEvent_SetTextW(IntPtr pEvent, string pszText);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_GetSMPTEOffset(IntPtr pEvent, out int pMode, out int pHour, out int pMin,
		                                                   out int pSec, out int pFrame, out int pSubFrame);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_SetSMPTEOffset(IntPtr pEvent, int lMode, int lHour, int lMin, int lSec, int lFrame,
		                                                   int lSubFrame);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_GetTempo(IntPtr pEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_SetTempo(IntPtr pEvent, int lTempo);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_GetTimeSignature(IntPtr pEvent, out int lnn, out int ldd, out int lcc, out int bb);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_SetTimeSignature(IntPtr pEvent, int lnn, int ldd, int lcc, int bb);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_GetKeySignature(IntPtr pEvent, out int psf, out int pmi);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_SetKeySignature(IntPtr pEvent, int lsf, int lmi);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_GetMIDIMessage(IntPtr pEvent, byte[] pMessage, int lLen);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_SetMIDIMessage(IntPtr pEvent, byte[] pMessage, int lLen);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_GetChannel(IntPtr pEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_SetChannel(IntPtr pEvent, int lCh);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_GetTime(IntPtr pEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_SetTimeSingle(IntPtr pEvent, int lTime);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_SetTime(IntPtr pEvent, int lTime);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_GetKey(IntPtr pEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_SetKey(IntPtr pEvent, int lKey);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_GetVelocity(IntPtr pEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_SetVelocity(IntPtr pEvent, int cVel);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_GetDuration(IntPtr pEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_SetDuration(IntPtr pEvent, int lDuration);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_GetBank(IntPtr pEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_GetBankMSB(IntPtr pEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_GetBankLSB(IntPtr pEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_SetBank(IntPtr pEvent, int lBank);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_SetBankMSB(IntPtr pEvent, int lBankMsb);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_SetBankLSB(IntPtr pEvent, int lBankLsb);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_GetNumber(IntPtr pEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_SetNumber(IntPtr pEvent, int cNum);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_GetValue(IntPtr pEvent);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIEvent_SetValue(IntPtr pEvent, int nVal);


		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_GetNextEvent(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_GetPrevEvent(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_GetNextSameKindEvent(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_GetPrevSameKindEvent(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_GetParent(IntPtr pMidiEvent);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_ToStringExW(IntPtr pMidiEvent, char[] pBuf, int lLen, int lFlags);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIEvent_ToStringW(IntPtr pMidiEvent, char[] pBuf, int lLen);

		#endregion

		private IntPtr midiEvent;

		/// <summary>
		/// この MIDI トラックのポインターを取得します。
		/// </summary>
		internal IntPtr Pointer
		{
			get { return this.midiEvent; }
		}

		#region プロパティ

		private static int MaxTempo
		{
			get { return 60000000; }
		}

		/// <summary>
		/// このイベントの次にあるイベントを取得します。
		/// </summary>
		internal MidiEvent NextEvent
		{
			get
			{
				var ev = MIDIEvent_GetNextEvent(this.midiEvent);
				return ev != IntPtr.Zero ? new MidiEvent(ev) : null;
			}
		}

		/// <summary>
		/// このイベントの前にあるイベントを取得します。
		/// </summary>
		internal MidiEvent PreviousEvent
		{
			get
			{
				var ev = MIDIEvent_GetPrevEvent(this.midiEvent);
				return ev != IntPtr.Zero ? new MidiEvent(ev) : null;
			}
		}

		/// <summary>
		/// このイベントの開始時刻を取得または設定します。
		/// </summary>
		public int StartTime
		{
			get { return MIDIEvent_GetTime(this.midiEvent); }
			set
			{
				if (MIDIEvent_SetTime(this.midiEvent, value) == 0)
					throw new InvalidOperationException("MIDI イベントの開始時間の設定に失敗しました。");
			}
		}

		/// <summary>
		/// このイベントの種類を取得します。
		/// </summary>
		public int TypeNumber
		{
			get { return MIDIEvent_GetKind(this.midiEvent); }
		}

		/// <summary>
		/// このイベントの種類を取得します。
		/// </summary>
		public MidiEventType EventType
		{
			get { return CheckEventType(); }
		}

		/// <summary>
		/// このイベントのデータ長を取得します。
		/// </summary>
		public int DataLength
		{
			get { return MIDIEvent_GetLen(this.midiEvent); }
		}

		/// <summary>
		/// このイベントの生データを取得します。
		/// </summary>
		public byte[] RawData
		{
			get
			{
				var data = new byte[this.DataLength];
				MIDIEvent_GetMIDIMessage(this.midiEvent, data, this.DataLength);

				return data;
			}
		}

		#region 各イベント依存プロパティ

		/// <summary>
		/// このイベントの文字列データを取得または設定します。
		/// </summary>
		public string Text
		{
			get
			{
				var data = new char[255];
				MIDIEvent_GetTextW(this.midiEvent, data, data.Length);
				return new string(data).Trim('\0');
			}
			set
			{
				if (MIDIEvent_SetTextW(this.midiEvent, value) == 0)
					throw new InvalidOperationException("文字列データの設定に失敗しました。");
			}
		}

		/// <summary>
		/// このイベントのテンポ データを取得または設定します。
		/// </summary>
		public double Tempo
		{
			get { return (double)MidiEvent.MaxTempo / MIDIEvent_GetTempo(this.midiEvent); }
			set
			{
				var t = (int)(MidiEvent.MaxTempo / value);
				if (MIDIEvent_SetTempo(this.midiEvent, t) == 0)
					throw new InvalidOperationException("テンポの設定に失敗しました。");
			}
		}

		/// <summary>
		/// このイベントのチャンネル データを取得または設定します。
		/// </summary>
		public int Channel
		{
			get { return MIDIEvent_GetChannel(this.midiEvent); }
			set
			{
				if (MIDIEvent_SetChannel(this.midiEvent, value) == 0)
					throw new InvalidOperationException("チャンネルの設定に失敗しました。");
			}
		}

		/// <summary>
		/// このイベントのキー データを取得または設定します。
		/// </summary>
		public int Key
		{
			get { return MIDIEvent_GetKey(this.midiEvent); }
			set
			{
				if (MIDIEvent_SetKey(this.midiEvent, value) == 0)
					throw new InvalidOperationException("キーの設定に失敗しました。");
			}
		}

		/// <summary>
		/// このイベントのベロシティ データを取得または設定します。
		/// </summary>
		public int Velocity
		{
			get { return MIDIEvent_GetVelocity(this.midiEvent); }
			set
			{
				if (MIDIEvent_SetVelocity(this.midiEvent, value) == 0)
					throw new InvalidOperationException("ベロシティの設定に失敗しました。");
			}
		}

		/// <summary>
		/// このイベントの音の長さを取得または設定します。
		/// </summary>
		public int Duration
		{
			get { return MIDIEvent_GetVelocity(this.midiEvent); }
			set
			{
				if (MIDIEvent_SetVelocity(this.midiEvent, value) == 0)
					throw new InvalidOperationException("音の長さの設定に失敗しました。");
			}
		}

		/// <summary>
		/// このイベントの番号を取得または設定します。
		/// </summary>
		public int Number
		{
			get { return MIDIEvent_GetNumber(this.midiEvent); }
			set
			{
				if (MIDIEvent_SetNumber(this.midiEvent, value) == 0)
					throw new InvalidOperationException("番号の設定に失敗しました。");
			}
		}

		/// <summary>
		/// このイベントの設定値を取得または設定します。
		/// </summary>
		public int Value
		{
			get { return MIDIEvent_GetValue(this.midiEvent); }
			set
			{
				if (MIDIEvent_SetValue(this.midiEvent, value) == 0)
					throw new InvalidOperationException("設定値の設定に失敗しました。");
			}
		}

		/// <summary>
		/// このイベントのバンク データを取得または設定します。
		/// </summary>
		public int Bank
		{
			get { return MIDIEvent_GetBank(this.midiEvent); }
			set
			{
				if (MIDIEvent_SetBank(this.midiEvent, value) == 0)
					throw new InvalidOperationException("バンクの設定に失敗しました。");
			}
		}

		public int BankMsb
		{
			get { return MIDIEvent_GetBankMSB(this.midiEvent); }
			set
			{
				if (MIDIEvent_SetBankMSB(this.midiEvent, value) == 0)
					throw new InvalidOperationException("バンク MSB の設定に失敗しました。");
			}
		}

		public int BankLsb
		{
			get { return MIDIEvent_GetBankLSB(this.midiEvent); }
			set
			{
				if (MIDIEvent_SetBankLSB(this.midiEvent, value) == 0)
					throw new InvalidOperationException("バンク LSB の設定に失敗しました。");
			}
		}

		#endregion

		#endregion

		protected MidiEvent(IntPtr midiEvent)
		{
			this.midiEvent = midiEvent;
		}

		#region Create

		internal static MidiEvent Create(IntPtr data)
		{
			if (data == IntPtr.Zero)
				throw new InvalidOperationException("MIDI イベントの作成に失敗しました。");

			return new MidiEvent(data);
		}

		public static MidiEvent CreateSequenceNumber(int time, int sequenceNumber)
		{
			var data = MIDIEvent_CreateSequenceNumber(time, sequenceNumber);

			return Create(data);
		}

		public static MidiEvent CreateTextEvent(int time, string text)
		{
			var data = MIDIEvent_CreateTextEventW(time, text);

			return Create(data);
		}

		public static MidiEvent CreateCopyrightNotice(int time, string text)
		{
			var data = MIDIEvent_CreateCopyrightNoticeW(time, text);

			return Create(data);
		}

		public static MidiEvent CreateTrackName(int time, string text)
		{
			var data = MIDIEvent_CreateTrackNameW(time, text);

			return Create(data);
		}

		public static MidiEvent CreateInstrumentName(int time, string text)
		{
			var data = MIDIEvent_CreateInstrumentNameW(time, text);

			return Create(data);
		}

		public static MidiEvent CreateLyric(int time, string text)
		{
			var data = MIDIEvent_CreateLyricW(time, text);

			return Create(data);
		}

		public static MidiEvent CreateMarker(int time, string text)
		{
			var data = MIDIEvent_CreateMakerW(time, text);

			return Create(data);
		}

		public static MidiEvent CreateCuePoint(int time, string text)
		{
			var data = MIDIEvent_CreateCuePointW(time, text);

			return Create(data);
		}

		public static MidiEvent CreateProgramName(int time, string text)
		{
			var data = MIDIEvent_CreateProgramNameW(time, text);

			return Create(data);
		}

		public static MidiEvent CreateDeviceName(int time, string text)
		{
			var data = MIDIEvent_CreateDeviceNameW(time, text);

			return Create(data);
		}

		public static MidiEvent CreateChannelPrefix(int time, int channel)
		{
			var data = MIDIEvent_CreateChannelPrefix(time, channel);

			return Create(data);
		}

		public static MidiEvent CreatePortPrefix(int time, int number)
		{
			var data = MIDIEvent_CreatePortPrefix(time, number);

			return Create(data);
		}

		public static MidiEvent CreateEndOfTrack(int time)
		{
			var data = MIDIEvent_CreateEndofTrack(time);

			return Create(data);
		}

		public static MidiEvent CreateTempo(int time, double tempo)
		{
			var t = (int)(MidiEvent.MaxTempo / tempo);
			var data = MIDIEvent_CreateTempo(time, t);

			return Create(data);
		}

		public static MidiEvent CreateSmpteOffset(int time, SmpteMode mode, int hour, int min, int sec, int frame, int subFrame)
		{
			var data = MIDIEvent_CreateSMPTEOffset(time, (int)mode, hour, min, sec, frame, subFrame);

			return Create(data);
		}

		/// <summary>
		/// 拍子記号イベントを作成します。
		/// </summary>
		/// <param name="time">作成する位置の Tick 数</param>
		/// <param name="numerator">拍子記号の分子</param>
		/// <param name="denominator">拍子記号の分母。通常は 1, 4, 8, 16, 32 のどれかが入ります。</param>
		/// <param name="timebase">分解能</param>
		/// <returns></returns>
		public static MidiEvent CreateMeter(int time, int numerator, int denominator, int timebase)
		{
			// 分母を2のべき乗に変更
			var log = (int)Math.Log(denominator, 2);
			// 32分音符の数を分母から算出
			var note32 = denominator / 32;
			// 分解能を4倍 (分解能は四分音符あたりの Tick 数なので、全音符の Tick 数に換算) して分母の音価で割る
			var clock = (timebase * 4) / denominator;

			var data = MIDIEvent_CreateTimeSignature(time, numerator, log, clock, note32);

			return Create(data);
		}

		/// <summary>
		/// 調号イベントを作成します。
		/// </summary>
		/// <param name="time">作成する位置の Tick 数</param>
		/// <param name="key">変化記号の数。0 を起点として負の方向にフラット (♭) の数、正の方向にシャープ (♯) の数を表します。 -7 ～ 7 の範囲で設定します。</param>
		/// <param name="signature"></param>
		/// <returns></returns>
		public static MidiEvent CreateKey(int time, int key, KeySignature signature)
		{
			if (!(-7 < key && key < 7))
				throw new ArgumentException("変化記号の数は -7 ～ 7 の範囲で指定してください。", "key");

			var data = MIDIEvent_CreateKeySignature(time, key, (int)signature);

			return Create(data);
		}

		public static MidiEvent CreateSequencerSpecific(int time, char[] message)
		{
			var data = MIDIEvent_CreateSequencerSpecific(time, message, message.Length);

			return Create(data);
		}

		/// <summary>
		/// ノート オフ イベントを生成します。
		/// </summary>
		/// <param name="time">作成する位置の Tick 数</param>
		/// <param name="channel">チャンネル番号</param>
		/// <param name="note">ノート番号</param>
		/// <param name="velocity">ベロシティ</param>
		/// <returns></returns>
		public static MidiEvent CreateNoteOff(int time, int channel, int note, int velocity)
		{
			var data = MIDIEvent_CreateNoteOff(time, channel, note, velocity);

			return Create(data);
		}

		/// <summary>
		/// ノート オン イベントを生成します。
		/// </summary>
		/// <param name="time">作成する位置の Tick 数</param>
		/// <param name="channel">チャンネル番号</param>
		/// <param name="note">ノート番号</param>
		/// <param name="velocity">ベロシティ</param>
		/// <returns></returns>
		public static MidiEvent CreateNoteOn(int time, int channel, int note, int velocity)
		{
			var data = MIDIEvent_CreateNoteOn(time, channel, note, velocity);

			return Create(data);
		}

		public static MidiEvent CreateKeyAftertouch(int time, int channel, int note, int value)
		{
			var data = MIDIEvent_CreateKeyAftertouch(time, channel, note, value);

			return Create(data);
		}

		/// <summary>
		/// コントロール チェンジ イベントを生成します。
		/// </summary>
		/// <param name="time">作成する位置の Tick 数</param>
		/// <param name="channel">チャンネル番号</param>
		/// <param name="number">コントロール ナンバー</param>
		/// <param name="value">設定値</param>
		/// <returns></returns>
		public static MidiEvent CreateControlChange(int time, int channel, int number, int value)
		{
			var data = MIDIEvent_CreateControlChange(time, channel, number, value);

			return Create(data);
		}

		/// <summary>
		/// プログラム チェンジ イベントを生成します。
		/// </summary>
		/// <param name="time">作成する位置の Tick 数</param>
		/// <param name="channel">チャンネル番号</param>
		/// <param name="number">プログラム ナンバー</param>
		/// <returns></returns>
		public static MidiEvent CreateProgramChange(int time, int channel, int number)
		{
			var data = MIDIEvent_CreateProgramChange(time, channel, number);

			return Create(data);
		}

		public static MidiEvent CreateChannelAftertouch(int time, int channel, int value)
		{
			var data = MIDIEvent_CreateChannelAftertouch(time, channel, value);

			return Create(data);
		}

		/// <summary>
		/// ピッチ ベンド イベントを生成します。
		/// </summary>
		/// <param name="time">作成する位置の Tick 数</param>
		/// <param name="channel">チャンネル番号</param>
		/// <param name="value">設定値</param>
		/// <returns></returns>
		public static MidiEvent CreatePitchBend(int time, int channel, int value)
		{
			var data = MIDIEvent_CreatePitchBend(time, channel, value);

			return Create(data);
		}

		/// <summary>
		/// システム エクスクルーシブ イベントを生成します。
		/// </summary>
		/// <param name="time">作成する位置の Tick 数</param>
		/// <param name="message">システム エクスクルーシブ メッセージ</param>
		/// <returns></returns>
		public static MidiEvent CreateSystemExclusive(int time, byte[] message)
		{
			var data = MIDIEvent_CreateSysEx(time, message, message.Length);

			return Create(data);
		}

		/// <summary>
		/// ノート オン イベントと、ノート オフ イベントを結合して生成します。
		/// </summary>
		/// <param name="time">作成する位置の Tick 数</param>
		/// <param name="channel">チャンネル番号</param>
		/// <param name="note">ノート番号</param>
		/// <param name="velocity1">ノート オン イベントのベロシティ</param>
		/// <param name="velocity2">ノート オフ イベントのベロシティ</param>
		/// <param name="duration">音の長さを示す Tick 数</param>
		/// <returns></returns>
		public static MidiEvent CreateNoteOnNoteOff(int time, int channel, int note, int velocity1, int velocity2, int duration)
		{
			var data = MIDIEvent_CreateNoteOnNoteOff(time, channel, note, velocity1, velocity2, duration);

			return Create(data);
		}

		/// <summary>
		/// ノート オン イベントと、ベロシティが 0 のノート オン イベントを結合して生成します。
		/// </summary>
		/// <param name="time">作成する位置の Tick 数</param>
		/// <param name="channel">チャンネル番号</param>
		/// <param name="note">ノート番号</param>
		/// <param name="velocity">ノート オン イベントのベロシティ</param>
		/// <param name="duration">音の長さを示す Tick 数</param>
		/// <returns></returns>
		public static MidiEvent CreateNote(int time, int channel, int note, int velocity, int duration)
		{
			var data = MIDIEvent_CreateNote(time, channel, note, velocity, duration);

			return Create(data);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="time">作成する位置の Tick 数</param>
		/// <param name="channel">チャンネル番号</param>
		/// <param name="bank">バンク番号</param>
		/// <param name="number">パッチ番号</param>
		/// <returns></returns>
		[Obsolete]
		public static MidiEvent CreatePatchChange(int time, int channel, int bank, int number)
		{
			var data = MIDIEvent_CreatePatchChange(time, channel, bank, number);

			return Create(data);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="time">作成する位置の Tick 数</param>
		/// <param name="channel">チャンネル番号</param>
		/// <param name="bank">バンク番号</param>
		/// <param name="value">設定値</param>
		/// <returns></returns>
		[Obsolete]
		public static MidiEvent CreateRpnChange(int time, int channel, int bank, int value)
		{
			var data = MIDIEvent_CreateRPNChange(time, channel, bank, value);

			return Create(data);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="time">作成する位置の Tick 数</param>
		/// <param name="channel">チャンネル番号</param>
		/// <param name="bank">バンク番号</param>
		/// <param name="value">設定値</param>
		/// <returns></returns>
		[Obsolete]
		public static MidiEvent CreateNrpnChange(int time, int channel, int bank, int value)
		{
			var data = MIDIEvent_CreateNRPNChange(time, channel, bank, value);

			return Create(data);
		}

		#endregion

		/// <summary>
		/// このイベントと結合できるイベントを探し、結合します。できなかった場合は何も実行しません。
		/// </summary>
		public void Combine()
		{
			MIDIEvent_Combine(this.midiEvent);
		}

		/// <summary>
		/// このイベントが別のイベントと結合している場合、結合を解除します。
		/// </summary>
		public void Split()
		{
			if (MIDIEvent_IsCombined(this.midiEvent) == 1)
				MIDIEvent_Chop(this.midiEvent);
		}

		public void Close()
		{
			if (this.midiEvent != IntPtr.Zero)
				MIDIEvent_Delete(this.midiEvent);

			this.midiEvent = IntPtr.Zero;
		}

		public void Dispose()
		{
			Close();
		}

		public override string ToString()
		{
			var status = string.Empty;

			switch ((MidiEventType)this.TypeNumber)
			{
				case MidiEventType.NoteOn:
				case MidiEventType.NoteOff:
					status = string.Format("<{0}> Ch.{1} -> Note: {2}, Vel: {3}", this.StartTime, this.Channel, this.Key, this.Velocity);
					break;
				case MidiEventType.ProgramChange:
					status = string.Format("<{0}> Ch.{1} -> PC#{2}", this.StartTime, this.Channel, this.Number);
					break;
				case MidiEventType.Tempo:
					status = string.Format("<{0}> Tempo: {1}", this.StartTime, this.Tempo);
					break;
				default:
					status = string.Format("<{0}> {1}", this.StartTime, string.Join(" ", this.RawData));
					break;
			}

			return string.Format("[{0}] {1}", (MidiEventType)this.TypeNumber, status);
		}

		private MidiEventType CheckEventType()
		{
			if (MIDIEvent_IsChannelAftertouch(this.midiEvent) == 1)
				return MidiEventType.ChannelAfterTouch;
			if (MIDIEvent_IsChannelPrefix(this.midiEvent) == 1)
				return MidiEventType.ChannelPrefix;
			if (MIDIEvent_IsControlChange(this.midiEvent) == 1)
				return MidiEventType.ControlChange;
			if (MIDIEvent_IsCopyrightNotice(this.midiEvent) == 1)
				return MidiEventType.CopyrightNotice;
			if (MIDIEvent_IsCuePoint(this.midiEvent) == 1)
				return MidiEventType.CuePoint;
			if (MIDIEvent_IsDeviceName(this.midiEvent) == 1)
				return MidiEventType.DeviceName;
			if (MIDIEvent_IsEndofTrack(this.midiEvent) == 1)
				return MidiEventType.EndofTrack;
			if (MIDIEvent_IsInstrumentName(this.midiEvent) == 1)
				return MidiEventType.InstrumentName;
			if (MIDIEvent_IsKeyAftertouch(this.midiEvent) == 1)
				return MidiEventType.KeyAfterTouch;
			if (MIDIEvent_IsKeySignature(this.midiEvent) == 1)
				return MidiEventType.KeySignature;
			if (MIDIEvent_IsLyric(this.midiEvent) == 1)
				return MidiEventType.Lyric;
			if (MIDIEvent_IsMarker(this.midiEvent) == 1)
				return MidiEventType.Marker;
			if (MIDIEvent_IsNoteOff(this.midiEvent) == 1)
				return MidiEventType.NoteOff;
			if (MIDIEvent_IsNoteOn(this.midiEvent) == 1)
				return MidiEventType.NoteOn;
			if (MIDIEvent_IsPitchBend(this.midiEvent) == 1)
				return MidiEventType.PitchBend;
			if (MIDIEvent_IsPortPrefix(this.midiEvent) == 1)
				return MidiEventType.PortPrefix;
			if (MIDIEvent_IsProgramChange(this.midiEvent) == 1)
				return MidiEventType.ProgramChange;
			if (MIDIEvent_IsProgramName(this.midiEvent) == 1)
				return MidiEventType.ProgramName;
			if (MIDIEvent_IsSequenceNumber(this.midiEvent) == 1)
				return MidiEventType.SequenceNumber;
			if (MIDIEvent_IsSequencerSpecific(this.midiEvent) == 1)
				return MidiEventType.SequencerPrecific;
			if (MIDIEvent_IsSMPTEOffset(this.midiEvent) == 1)
				return MidiEventType.SmpteOffSet;
			if (MIDIEvent_IsSysExEvent(this.midiEvent) == 1)
				return MidiEventType.SysExStart;
			if (MIDIEvent_IsTempo(this.midiEvent) == 1)
				return MidiEventType.Tempo;
			if (MIDIEvent_IsTextEvent(this.midiEvent) == 1)
				return MidiEventType.TextEvent;
			if (MIDIEvent_IsTimeSignature(this.midiEvent) == 1)
				return MidiEventType.TimeSignature;
			if (MIDIEvent_IsTrackName(this.midiEvent) == 1)
				return MidiEventType.TrackName;

			return MidiEventType.Unknown;
		}
	}
}
