﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.Data
{
	public struct MidiTime
	{
		public int Measure { get; set; }
		public int Beat { get; set; }
		public int Tick { get; set; }
	}
}
