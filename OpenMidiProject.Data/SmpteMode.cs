﻿namespace OpenMidiProject.Data
{
	/// <summary>
	/// SMPTE モードを定義します。
	/// </summary>
	public enum SmpteMode
	{
		Smpte24=0,
		Smpte25,
		Smpte30D,
		Smpte30N
	}
}