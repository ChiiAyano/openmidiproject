﻿namespace OpenMidiProject.Data
{
	/// <summary>
	/// 調の種類を定義します。
	/// </summary>
	public enum KeySignature
	{
		/// <summary>
		/// 長調
		/// </summary>
		Major,
		/// <summary>
		/// 短調
		/// </summary>
		Minor
	}
}