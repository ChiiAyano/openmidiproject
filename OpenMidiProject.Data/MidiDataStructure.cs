﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.Data
{
	[StructLayout(LayoutKind.Sequential)]
	public struct MidiDataStructure
	{
		public uint m_lFormat; /* SMFフォーマット(0/1) */
		public uint m_lNumTrack; /* トラック数(0～∞) */
		public uint m_lTimeBase; /* タイムベース(例：120) */
		public IntPtr m_pFirstTrack; /* 最初のトラックへのポインタ(なければNULL) */
		public IntPtr m_pLastTrack; /* 最後のトラックへのポインタ(なければNULL) */
		public IntPtr m_pNextSeq; /* 次のシーケンスへのポインタ(なければNULL) */
		public IntPtr m_pPrevSeq; /* 前のシーケンスへのポインタ(なければNULL) */
		public IntPtr m_pParent; /* 親(常にNULL。将来ソングリストをサポート) */
		public int m_lReserved1; /* 予約領域1(使用禁止) */
		public int m_lReserved2; /* 予約領域2(使用禁止) */
		public int m_lReserved3; /* 予約領域3(使用禁止) */
		public int m_lReserved4; /* 予約領域4(使用禁止) */
		public int m_lUser1; /* ユーザー用自由領域1(未使用) */
		public int m_lUser2; /* ユーザー用自由領域2(未使用) */
		public int m_lUser3; /* ユーザー用自由領域3(未使用) */
		public int m_lUserFlag; /* ユーザー用自由領域4(未使用) */
	}
}
