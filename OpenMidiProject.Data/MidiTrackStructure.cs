﻿using System;
using System.Runtime.InteropServices;
namespace OpenMidiProject.Data
{
	[StructLayout(LayoutKind.Sequential)]
	public struct MidiTrackStructure
	{
		public int m_lTempIndex; /* このトラックの一時的なインデックス(0から始まる) */
		public int m_lNumEvent; /* トラック内のイベント数 */
		public IntPtr m_pFirstEvent; /* 最初のイベントへのポインタ(なければNULL) */
		public IntPtr m_pLastEvent; /* 最後のイベントへのポインタ(なければNULL) */
		public IntPtr m_pPrevTrack; /* 前のトラックへのポインタ(なければNULL) */
		public IntPtr m_pNextTrack; /* 次のトラックへのポインタ(なければNULL) */
		public IntPtr m_pParent; /* 親(MIDIDataオブジェクト)へのポインタ */
		public int m_lInputOn; /* 入力(0=OFF, 1=On) */
		public int m_lInputPort; /* 入力ポート(-1=n/a, 0～15=ポート番号) */
		public int m_lInputChannel; /* 入力チャンネル(-1=n/1, 0～15=チャンネル番号) */
		public int m_lOutputOn; /* 出力(0=OFF, 1=On) */
		public int m_lOutputPort; /* 出力ポート(-1=n/a, 0～15=ポート番号) */
		public int m_lOutputChannel; /* 出力チャンネル(-1=n/1, 0～15=チャンネル番号) */
		public int m_lTimePlus; /* タイム+ */
		public int m_lKeyPlus; /* キー+ */
		public int m_lVelocityPlus; /* ベロシティ+ */
		public int m_lViewMode; /* 表示モード(0=通常、1=ドラム) */
		public int m_lForeColor; /* 前景色 */
		public int m_lBackColor; /* 背景色 */
		public int m_lReserved1; /* 予約領域1(使用禁止) */
		public int m_lReserved2; /* 予約領域2(使用禁止) */
		public int m_lReserved3; /* 予約領域3(使用禁止) */
		public int m_lReserved4; /* 予約領域4(使用禁止) */
		public int m_lUser1; /* ユーザー用自由領域1(未使用) */
		public int m_lUser2; /* ユーザー用自由領域2(未使用) */
		public int m_lUser3; /* ユーザー用自由領域3(未使用) */
		public int m_lUserFlag; /* ユーザー用自由領域4(未使用) */
	}
}