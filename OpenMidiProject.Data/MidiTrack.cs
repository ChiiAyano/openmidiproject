﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.Data
{
    public class MidiTrack : IDisposable
	{
		#region API 定義

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_GetNumEvent(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDITrack_GetFirstEvent(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDITrack_GetLastEvent(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDITrack_GetFirstKindEvent(IntPtr pMidiTrack, int lKind);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDITrack_GetLastKindEvent(IntPtr pMidiTrack, int lKind);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDITrack_GetNextTrack(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDITrack_GetPrevTrack(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDITrack_GetParent(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_CountEvent(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_GetBeginTime(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_GetEndTime(IntPtr pMidiTrack);

	    [DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
	    private static extern IntPtr MIDITrack_GetNameW(IntPtr pMidiTrack, char[] pBuf, int lLen);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_GetInputOn(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_GetInputPort(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_GetInputChannel(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_GetOutputOn(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_GetOutputPort(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_GetOutputChannel(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_GetTimePlus(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_GetKeyPlus(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_GetVelocityPlus(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_GetViewMode(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_GetForeColor(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_GetBackColor(IntPtr pMidiTrack);

	    [DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
	    private static extern int MIDITrack_SetNameW(IntPtr pMidiTrack, string pszText);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_SetInputOn(IntPtr pMidiTrack, int lInputOn);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_SetInputPort(IntPtr pMidiTrack, int lInputPort);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_SetInputChannel(IntPtr pMidiTrack, int lInputChannel);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_SetOutputOn(IntPtr pMidiTrack, int lOutputOn);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_SetOutputPort(IntPtr pMidiTrack, int lOutputPort);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_SetOutputChannel(IntPtr pMidiTrack, int lOutputChannel);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_SetTimePlus(IntPtr pMidiTrack, int lTimePlus);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_SetKeyPlus(IntPtr pMidiTrack, int lKeyPlus);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_SetVelocityPlus(IntPtr pMidiTrack, int lVelocityPlus);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_SetViewMode(IntPtr pMidiTrack, int lMode);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_SetForeColor(IntPtr pMidiTrack, int lForeColor);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_SetBackColor(IntPtr pMidiTrack, int lBackColor);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_GetXFVersion(IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern void MIDITrack_Delete(IntPtr pMidiTrack);

	    [DllImport("MIDIData.dll")]
	    private static extern IntPtr MIDITrack_Create();

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDITrack_Create(IntPtr pTrack);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_InsertSingleEventAfter(IntPtr pMidiTrack, IntPtr pEvent, IntPtr pTarget);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_InsertSingleEventBefore(IntPtr pMidiTrack, IntPtr pEvent, IntPtr pTarget);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_InsertEventAfter(IntPtr pMidiTrack, IntPtr pEvent, IntPtr pTarget);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_InsertEventBefore(IntPtr pMidiTrack, IntPtr pMidiEvent, IntPtr pTarget);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_InsertEvent(IntPtr pMidiTrack, IntPtr pEvent);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_InsertSequenceNumber(IntPtr pTrack, int lTime, int lNum);

	    [DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
	    private static extern int MIDITrack_InsertTextEventW(IntPtr pTrack, int lTime, string pszText);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDITrack_InsertCopyrightNoticeW(IntPtr pTrack, int lTime, string pszText);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDITrack_InsertTrackNameW(IntPtr pTrack, int lTime, string pszText);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDITrack_InsertInstrumentNameW(IntPtr pTrack, int lTime, string pszText);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDITrack_InsertLyricW(IntPtr pTrack, int lTime, string pszText);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDITrack_InsertMarkerW(IntPtr pTrack, int lTime, string pszText);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDITrack_InsertCuePointW(IntPtr pTrack, int lTime, string pszText);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDITrack_InsertProgramNameW(IntPtr pMidiTrack, int lTime, string pszText);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDITrack_InsertDeviceNameW(IntPtr pMidiTrack, int lTime, string pszText);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_InsertChannelPrefix(IntPtr pTrack, int lTime, int lCh);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_InsertPortPrefix(IntPtr pTrack, int lTime, int lPort);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_InsertEndofTrack(IntPtr pMidiTrack, int lTime);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_InsertTempo(IntPtr pMidiTrack, int lTime, int lTempo);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_InsertSMPTEOffset(IntPtr pTrack, int lTime, int lMode, int lHour, int lMin,
	                                                          int lSec, int lFrame, int lSubFrame);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_InsertTimeSignature(IntPtr pMidiTrack, int lTime, int lnn, int ldd, int lcc,
	                                                            int lbb);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_InsertKeySignature(IntPtr pMidiTrack, int lTime, int lsf, int lmi);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_InsertSequencerSpecific(IntPtr pMidiTrack, int lTime, byte[] pBuf, int lLen);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_InsertNoteOff(IntPtr pMidiTrack, int lTime, int lCh, int lKey, int lVel);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_InsertNoteOn(IntPtr pMidiTrack, int lTime, int lCh, int lKey, int lVel);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_InsertNote(IntPtr pMidiTrack, int lTime, int lCh, int lKey, int lVel, int lDur);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_InsertKeyAftertouch(IntPtr pMidiTrack, int lTime, int lCh, int lKey, int lVal);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_InsertControlChange(IntPtr pMidiTrack, int lTime, int lCh, int lKey, int lVal);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_InsertRPNChange(IntPtr pMidiTrack, int lTime, int lCh, int lKey, int lVal);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_InsertNRPNChange(IntPtr pMidiTrack, int lTime, int lCh, int lKey, int lVal);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_InsertProgramChange(IntPtr pMidiTrack, int lTime, int lCh, int lNum);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_InsertPatchChange(IntPtr pMidiTrack, int lTime, int lCh, int lBank, int lNum);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_InsertChannelAftertouch(IntPtr pMidiTrack, int lTime, int lCh, int lVal);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_InsertPitchBend(IntPtr pMidiTrack, int lTime, int lCh, int lVal);

		[DllImport("MIDIData.dll")]
		private static extern int MIDITrack_InsertSysExEvent(IntPtr pMidiTrack, int lTime, byte[] pBuf, int lLen);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_RemoveSingleEvent(IntPtr pTrack, IntPtr pEvent);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_RemoveEvent(IntPtr pTrack, IntPtr pEvent);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_IsFloating(IntPtr pMidiTrack);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_CheckSetupTrack(IntPtr pMidiTrack);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_CHeckNonSetupTrack(IntPtr pMidiTrack);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_TimeToMillisec(IntPtr pMidiTrack, int lTime);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_MillisecToTime(IntPtr pMidiTrack, int lMillisec);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_BreakTimeEx(IntPtr pMidiTrack, int lTime, out int pMeasure, out int pBeat,
	                                                    out int pTick, out int pnn, out int pdd, out int pcc, out int pbb);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_BreakTime(IntPtr pMidiTrack, int lTime, out int pMeasure, out int pBeat,
	                                                  out int pTick);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_MakeTimeEx(IntPtr pMidiTrack, int lMeasure, int lBeat, int lTick, ref int pTime,
	                                                   ref int pnn, ref int pdd, ref int pcc, ref int pbb);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_MakeTime(IntPtr pMidiTrack, int lMeasure, int lBeat, int lTick, ref int pTime);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_FindTempo(IntPtr pMidiTrack, int lTime, out int pTempo);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_FindTimeSignature(IntPtr pMidiTrack, int lTime, out int pnn, out int pdd,
	                                                          out int pcc, out int pbb);

	    [DllImport("MIDIData.dll")]
	    private static extern int MIDITrack_FindKeySignature(IntPtr pMidiTrack, int lTime, out int psf, out int pmi);


	    #endregion

	    private IntPtr midiTrack;

		/// <summary>
		/// この MIDI トラックのポインターを取得します。
		/// </summary>
	    internal IntPtr Pointer
	    {
		    get { return this.midiTrack; }
	    }

	    #region プロパティ

		/// <summary>
		/// このトラックより前にあるトラックを取得します。
		/// </summary>
	    internal MidiTrack PreviousTrack
	    {
		    get
		    {
			    var track = MIDITrack_GetPrevTrack(this.midiTrack);
			    return track != IntPtr.Zero ? new MidiTrack(track) : null;
		    }
	    }

		/// <summary>
		/// このトラックの次にあるトラックを取得します。
		/// </summary>
	    internal MidiTrack NextTrack
	    {
		    get
		    {
			    var track = MIDITrack_GetNextTrack(this.midiTrack);
			    return track != IntPtr.Zero ? new MidiTrack(track) : null;
		    }
	    }

	    /// <summary>
		/// この MIDI トラックが、MIDI データに属していないか取得します。
		/// </summary>
	    public bool IsFloating
	    {
		    get { return MIDITrack_IsFloating(this.midiTrack) == 1; }
	    }

		/// <summary>
		/// この MIDI トラックが、セットアップ トラックとして機能するトラックかを取得します。
		/// </summary>
	    public bool IsSetupTrack
	    {
		    get { return MIDITrack_CheckSetupTrack(this.midiTrack) == 1; }
	    }

		/// <summary>
		/// このトラックの開始時刻を取得します。
		/// </summary>
	    public int BeginTime
	    {
		    get { return MIDITrack_GetBeginTime(this.midiTrack); }
	    }

		/// <summary>
		/// このトラックの終了時刻を取得します。
		/// </summary>
	    public int EndTime
	    {
		    get { return MIDITrack_GetEndTime(this.midiTrack); }
	    }

	    /// <summary>
	    /// このトラックの名前を取得または設定します。
	    /// </summary>
	    public string TrackName
	    {
		    get
		    {
			    var name = new char[256];
			    MIDITrack_GetNameW(this.midiTrack, name, name.Length);
			    return new string(name).Trim('\0');
		    }
			set
			{
				if (MIDITrack_SetNameW(this.midiTrack, value) == 0)
					throw new InvalidOperationException("トラック名の設定に失敗しました。");
			}
	    }

	    public bool IsPlayOutput
	    {
		    get { return MIDITrack_GetOutputOn(this.midiTrack) == 1; }
		    set
		    {
			    if (MIDITrack_SetOutputOn(this.midiTrack, value ? 1 : 0) == 0)
				    throw new InvalidOperationException("設定に失敗しました。");
		    }
	    }

	    /// <summary>
		/// このトラックの出力ポートを取得または設定します。
		/// </summary>
	    public int OutputPort
	    {
		    get { return MIDITrack_GetOutputPort(this.midiTrack); }
			set
			{
				if (MIDITrack_SetOutputPort(this.midiTrack, value) == 0)
					throw new InvalidOperationException("出力ポートの設定に失敗しました。");
			}
	    }

		/// <summary>
		/// このトラックの出力チャンネルを取得または設定します。
		/// </summary>
	    public int OutputChannel
	    {
		    get { return MIDITrack_GetOutputChannel(this.midiTrack); }
		    set
		    {
			    if (MIDITrack_SetOutputChannel(this.midiTrack, value) == 0)
				    throw new InvalidOperationException("出力チャンネルの設定に失敗しました。");
		    }
	    }

		/// <summary>
		/// このトラックに含まれる MIDI イベントを列挙します。
		/// </summary>
	    public IEnumerable<MidiEvent> MidiEvents
	    {
		    get
		    {
			    var events = new List<MidiEvent>();
			    var firstEvent = MIDITrack_GetFirstEvent(this.midiTrack);

			    if (firstEvent == IntPtr.Zero)
				    return events;

			    var ev = MidiEvent.Create(firstEvent);
			    MidiEvent next;

			    events.Add(ev);
			    while ((next = ev.NextEvent) != null)
			    {
				    events.Add(next);
				    ev = next;
			    }

			    return events;
		    }
	    }

	    #endregion

		/// <summary>
		/// 新しい MIDI トラックを作成します。
		/// </summary>
		/// <returns></returns>
	    public static MidiTrack CreateNew()
	    {
		    var data = MIDITrack_Create();
			var track = new MidiTrack(data);
			
			// EndOfTrack挿入
			track.AddEvent(MidiEvent.CreateEndOfTrack(0));

		    return track;
	    }

		/// <summary>
		/// ポインターから MIDI トラックのインスタンスを生成します。
		/// </summary>
		/// <param name="midiTrack"></param>
		/// <returns></returns>
	    internal static MidiTrack Create(IntPtr midiTrack)
	    {
		    return new MidiTrack(midiTrack);
	    }

	    private MidiTrack(IntPtr midiTrack)
	    {
		    this.midiTrack = midiTrack;
	    }

		#region イベント挿入

		///// <summary>
		///// シーケンス番号イベントを生成し、トラックに挿入します。
		///// </summary>
		///// <param name="time">挿入する位置の Tick 数</param>
		///// <param name="number">シーケンス番号</param>
		//public void InsertSequenceNumber(int time, int number)
		//{
		//	if (MIDITrack_InsertSequenceNumber(this.midiTrack, time, number) == 0)
		//		throw new InvalidOperationException("シーケンス番号イベントの挿入に失敗しました。");
		//}

		///// <summary>
		///// テキスト イベントを生成し、トラックに挿入します。
		///// </summary>
		///// <param name="time">挿入する位置の Tick 数</param>
		///// <param name="text">テキスト</param>
		//public void InsertTextEvent(int time, string text)
		//{
		//	if (MIDITrack_InsertTextEventW(this.midiTrack, time, text) == 0)
		//		throw new InvalidOperationException("テキスト イベントの挿入に失敗しました。");
		//}

		///// <summary>
		///// 著作権情報イベントを生成し、トラックに挿入します。
		///// </summary>
		///// <param name="time">挿入する位置の Tick 数</param>
		///// <param name="text">テキスト</param>
		//public void InsertCopyrightNotice(int time, string text)
		//{
		//	if (MIDITrack_InsertCopyrightNoticeW(this.midiTrack, time, text) == 0)
		//		throw new InvalidOperationException("著作権情報イベントの挿入に失敗しました。");
		//}

		///// <summary>
		///// トラック名イベントを生成し、トラックに挿入します。
		///// </summary>
		///// <param name="time">挿入する位置の Tick 数</param>
		///// <param name="text">テキスト</param>
		//public void InsertTrackName(int time, string text)
		//{
		//	if (MIDITrack_InsertTrackNameW(this.midiTrack, time, text) == 0)
		//		throw new InvalidOperationException("トラック名イベントの挿入に失敗しました。");
		//}

		///// <summary>
		///// 楽器名イベントを生成し、トラックに挿入します。
		///// </summary>
		///// <param name="time">挿入する Tick 時間</param>
		///// <param name="text">テキスト</param>
		//public void InsertInstrumentName(int time, string text)
		//{
		//	if (MIDITrack_InsertInstrumentNameW(this.midiTrack, time, text) == 0)
		//		throw new InvalidOperationException("楽器名イベントの挿入に失敗しました。");
		//}

		///// <summary>
		///// 歌詞イベントを生成し、トラックに挿入します。
		///// </summary>
		///// <param name="time">挿入する位置の Tick 数</param>
		///// <param name="text">テキスト</param>
		//public void InsertLyric(int time, string text)
		//{
		//	if (MIDITrack_InsertLyricW(this.midiTrack, time, text) == 0)
		//		throw new InvalidOperationException("歌詞イベントの挿入に失敗しました。");
		//}

		///// <summary>
		///// マーカー イベントを生成し、トラックに挿入します。
		///// </summary>
		///// <param name="time">挿入する位置の Tick 数</param>
		///// <param name="text">テキスト</param>
		//public void InsertMarker(int time, string text)
		//{
		//	if (MIDITrack_InsertMarkerW(this.midiTrack, time, text) == 0)
		//		throw new InvalidOperationException("マーカー イベントの挿入に失敗しました。");
		//}

		///// <summary>
		///// キュー ポイント イベントを生成し、トラックに挿入します。
		///// </summary>
		///// <param name="time">挿入する位置の Tick 数</param>
		///// <param name="text">テキスト</param>
		//public void InsertCuePoint(int time, string text)
		//{
		//	if (MIDITrack_InsertCuePointW(this.midiTrack, time, text) == 0)
		//		throw new InvalidOperationException("キュー ポイント イベントの挿入に失敗しました。");
		//}

		///// <summary>
		///// プログラム名イベントを生成し、トラックに挿入します。
		///// </summary>
		///// <param name="time">挿入する位置の Tick 数</param>
		///// <param name="text">テキスト</param>
		//public void InsertProgramName(int time, string text)
		//{
		//	if (MIDITrack_InsertProgramNameW(this.midiTrack, time, text) == 0)
		//		throw new InvalidOperationException("プログラム名イベントの挿入に失敗しました。");
		//}

		///// <summary>
		///// デバイス名イベントを生成し、トラックに挿入します。
		///// </summary>
		///// <param name="time">挿入する位置の Tick 数</param>
		///// <param name="text">テキスト</param>
		//public void InsertDeviceName(int time, string text)
		//{
		//	if (MIDITrack_InsertDeviceNameW(this.midiTrack, time, text) == 0)
		//		throw new InvalidOperationException("デバイス名イベントの挿入に失敗しました。");
		//}

		///// <summary>
		///// チャンネル番号の指定を表すイベントを生成し、トラックに挿入します。
		///// </summary>
		///// <param name="time">挿入する位置の Tick 数</param>
		///// <param name="channel">チャンネル番号</param>
		//public void InsertChannelPrefix(int time, int channel)
		//{
		//	if (MIDITrack_InsertChannelPrefix(this.midiTrack, time, channel) == 0)
		//		throw new InvalidOperationException("チャンネル番号イベントの挿入に失敗しました。");
		//}

		///// <summary>
		///// ポートを指定するイベントを生成し、トラックに挿入します。
		///// </summary>
		///// <param name="time">挿入する位置の Tick 数</param>
		///// <param name="port">ポート</param>
		//public void InsertPortPrefix(int time, int port)
		//{
		//	if (MIDITrack_InsertPortPrefix(this.midiTrack, time, port) == 0)
		//		throw new InvalidOperationException("ポート指定イベントの挿入に失敗しました。");
		//}

		///// <summary>
		///// トラック終端イベントを生成し、トラックに挿入します。
		///// </summary>
		///// <param name="time"></param>
		//public void InsertEndOfTrack(int time)
		//{
		//	if (MIDITrack_InsertEndofTrack(this.midiTrack, time) == 0)
		//		throw new InvalidOperationException("トラック終端イベントの挿入に失敗しました。");
		//}

		//// SMPTEイベント省略

		///// <summary>
		///// 拍子記号イベントを作成し、トラックに挿入します。
		///// </summary>
		///// <param name="time">作成する位置の Tick 数</param>
		///// <param name="numerator">拍子記号の分子</param>
		///// <param name="denominator">拍子記号の分母。通常は 1, 4, 8, 16, 32 のどれかが入ります。</param>
		///// <param name="timebase">分解能</param>
		///// <returns></returns>
		//public void InsertMeterSignature(int time, int numerator, int denominator, int timebase)
		//{
		//	// 分母を2のべき乗に変更
		//	var log = (int)Math.Log(denominator, 2);
		//	// 32分音符の数を分母から算出
		//	var note32 = denominator / 32;
		//	// 分解能を4倍 (分解能は四分音符あたりの Tick 数なので、全音符の Tick 数に換算) して分母の音価で割る
		//	var clock = (timebase * 4) / denominator;

		//	if (MIDITrack_InsertTimeSignature(this.midiTrack, time, numerator, log, clock, note32) == 0)
		//		throw new InvalidOperationException("拍子記号イベントの挿入に失敗しました。");
		//}

		///// <summary>
		///// 調号イベントを作成し、トラックに挿入します。
		///// </summary>
		///// <param name="time">作成する位置の Tick 数</param>
		///// <param name="key">変化記号の数。0 を起点として負の方向にフラット (♭) の数、正の方向にシャープ (♯) の数を表します。 -7 ～ 7 の範囲で設定します。</param>
		///// <param name="signature"></param>
		///// <returns></returns>
		//public void InsertKeySignature(int time, int key, KeySignature signature)
		//{
		//	if (!(-7 < key && key < 7))
		//		throw new ArgumentException("変化記号の数は -7 ～ 7 の範囲で指定してください。", "key");

		//	if (MIDITrack_InsertKeySignature(this.midiTrack, time, key, (int)signature) == 0)
		//		throw new InvalidOperationException("調号イベントの挿入に失敗しました。");
		//}

		///// <summary>
		///// シーケンサー独自のイベントを生成し、トラックに挿入します。
		///// </summary>
		///// <param name="time">作成する位置の Tick 数</param>
		///// <param name="message"></param>
		//public void InsertSequencerSpecific(int time, byte[] message)
		//{
		//	if (MIDITrack_InsertSequencerSpecific(this.midiTrack, time, message, message.Length) == 0)
		//		throw new InvalidOperationException("シーケンサー独自イベントの挿入に失敗しました。");
		//}

		

	    #endregion

		/// <summary>
		/// MIDI イベントを追加します。
		/// </summary>
		/// <param name="ev"></param>
	    public void AddEvent(MidiEvent ev)
	    {
		    if (MIDITrack_InsertEvent(this.midiTrack, ev.Pointer) == 0)
			    throw new InvalidOperationException("MIDI イベントの追加に失敗しました。");
	    }

		/// <summary>
		/// MIDI イベントを追加します。
		/// </summary>
		/// <param name="ev"></param>
	    public void AddRangeEvent(IEnumerable<MidiEvent> ev)
	    {
		    foreach (var midiEvent in ev)
		    {
			    AddEvent(midiEvent);
		    }
	    }

	    /// <summary>
		/// 指定したイベントが結合できる場合、結合します。
		/// </summary>
		/// <param name="eventType"></param>
	    public void CombineEvent(MidiEventType eventType)
	    {
			var ev = MidiEvent.Create(MIDITrack_GetFirstEvent(this.midiTrack));

		    do
		    {
			    if (ev.TypeNumber == (int)eventType)
				    ev.Combine();
		    } while ((ev = ev.NextEvent) != null);
	    }

	    public void Close()
	    {
		    MIDITrack_Delete(this.midiTrack);
		    this.midiTrack = IntPtr.Zero;
	    }

	    public void Dispose()
	    {
		    Close();
	    }

		public override string ToString()
		{
			return string.Format("Port{0} Ch.{1} {2}", this.OutputPort, this.OutputChannel, this.TrackName);
		}

	    public MidiTrackStructure ToStructure()
	    {
		    return (MidiTrackStructure)Marshal.PtrToStructure(this.midiTrack, typeof(MidiTrackStructure));
	    }
	}
}
