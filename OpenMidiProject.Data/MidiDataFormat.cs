﻿namespace OpenMidiProject.Data
{
	/// <summary>
	/// SMF ファイル フォーマットを定義します。
	/// </summary>
	public enum MidiDataFormat
	{
		Format0 = 0x00,
		Format1 = 0x01,
		Format2 = 0x02
	}
}