﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.Data
{
	public class MidiData : IDisposable
	{
		#region API 定義

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_InsertTrackBefore(IntPtr pMidiData, IntPtr pMidiTrack, IntPtr pTarget);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_InsertTrackAfter(IntPtr pMidiData, IntPtr pMidiTrack, IntPtr pTarget);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_AddTrack(IntPtr pMidiData, IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_DuplicateTrack(IntPtr pMidiData, IntPtr pTrack);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_RemoveTrack(IntPtr pMidiData, IntPtr pMidiTrack);

		[DllImport("MIDIData.dll")]
		private static extern void MIDIData_Delete(IntPtr pMidiData);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIData_Create(int lFormat, int lNumTrack, int lTimeMode, int lResolution);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_GetFormat(IntPtr pMidiData);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_SetFormat(IntPtr pMidiData, int lFormat);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_GetTimeBase(IntPtr pMidiData, out int pMode, out int pResolution);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_GetTimeMode(IntPtr pMidiData);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_GetTimeResolution(IntPtr pMidiData);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_SetTimeBase(IntPtr pMidiData, int lMode, int lResolution);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_GetNumTrack(IntPtr pMidiData);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_CountTrack(IntPtr pMidiData);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_GetXFVersion(IntPtr pMidiData);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIData_GetFirstTrack(IntPtr pMidiData);

		[DllImport("MIDIData.dll")]
		private static extern IntPtr MIDIData_GetLastTrack(IntPtr pMidiData);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_GetBeginTime(IntPtr pMidiData);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_GetEndTime(IntPtr pMidiData);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIData_GetTitleW(IntPtr pMidiData, char[] pData, int lLen);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDIData_SetTitleW(IntPtr pMidiData, string pszData);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIData_GetSubTitleW(IntPtr pMidiData, char[] pData, int lLen);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDIData_SetSubTitleW(IntPtr pMidiData, string pszData);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIData_GetCopyrightW(IntPtr pMidiData, char[] pData, int lLen);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDIData_SetCopyrightW(IntPtr pMidiData, string pszData);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIData_GetCommentW(IntPtr pMidiData, char[] pData, int lLen);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDIData_SetCommentW(IntPtr pMidiData, string pszData);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_TimeToMillisec(IntPtr pMidiData, int lTime);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_MillisecToTime(IntPtr pMidiData, int lMillisec);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_BreakTime(IntPtr pMidiData, int lTime, out int pMeasure, out int pBeat,
		                                             out int pTick);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_BreakTimeEx(IntPtr pMidiData, int lTime, out int pMeasure, out int pBeat,
		                                               out int pTick, out int pnn, out int pdd, out int pcc, out int pbb);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_MakeTime(IntPtr pMidiData, int lMeasure, int lBeat, int lTick, ref int pTime);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_MakeTimeEx(IntPtr pMidiData, int lMeasure, int lBeat, int lTick, ref int pTime,
		                                              ref int pnn, ref int pdd, ref int pcc, ref int pbb);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_FindTempo(IntPtr pMidiData, int lTime, out int pTempo);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_FindTimeSignature(IntPtr pMidiData, int lTime, out int pnn, out int pdd,
		                                                     out int pcc, out int pbb);

		[DllImport("MIDIData.dll")]
		private static extern int MIDIData_FindKeySignature(IntPtr pMidiData, int lTime, out int psf, out int pmi);

		// 読み書き

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIData_LoadFromSMFW(string pszFileName);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDIData_SaveAsSMFW(IntPtr pMidiData, string pszFileName);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIData_LoadFromTextW(string pszFileName);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDIData_SaveAsTextW(IntPtr pMidiData, string pszFileName);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIData_LoadFromBinaryW(string pszFileName);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDIData_SaveAsBinaryW(IntPtr pMidiData, string pszFileName);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIData_LoadFromCherryW(string pszFileName);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDIData_SaveAsCherryW(IntPtr pMidiData, string pszFileName);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIData_LoadFromMIDICSVW(string pszFileName);

		[DllImport("MIDIData.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDIData_SaveAsMIDICSVW(IntPtr pMidiData, string pszFileName);

		#endregion

		private IntPtr midiData;

		#region プロパティ

		/// <summary>
		/// MIDI フォーマットの状態を取得または設定します。
		/// </summary>
		public MidiDataFormat Format
		{
			get { return (MidiDataFormat)MIDIData_GetFormat(this.midiData); }
			set
			{
				if (MIDIData_SetFormat(this.midiData, (int)value) == 0)
					throw new InvalidOperationException("MIDI フォーマットの設定に失敗しました。");
			}
		}

		/// <summary>
		/// タイム モードの状態を取得または設定します。
		/// </summary>
		public TimeMode TimeMode
		{
			get { return (TimeMode)MIDIData_GetTimeMode(this.midiData); }
			set
			{
				if (MIDIData_SetTimeBase(this.midiData, (int)value, this.Resolution) == 0)
					throw new InvalidOperationException("タイム モードの設定に失敗しました。");
			}
		}

		/// <summary>
		/// 分解能を取得または設定します。
		/// </summary>
		public int Resolution
		{
			get { return MIDIData_GetTimeResolution(this.midiData); }
			set
			{
				if (MIDIData_SetTimeBase(this.midiData, (int)this.TimeMode, value) == 0)
					throw new InvalidOperationException("分解能の設定に失敗しました。");
			}
		}

		/// <summary>
		/// 曲のタイトルを取得または設定します。
		/// </summary>
		public string Title
		{
			get
			{
				var title = new char[256];
				MIDIData_GetTitleW(this.midiData, title, title.Length);
				return new string(title).Trim('\0');
			}
			set { MIDIData_SetTitleW(this.midiData, value); }
		}

		/// <summary>
		/// 著作権表示を取得または設定します。
		/// </summary>
		public string Copyright
		{
			get
			{
				var copyright = new char[256];
				MIDIData_GetCopyrightW(this.midiData, copyright, copyright.Length);
				return new string(copyright).Trim('\0');
			}
			set { MIDIData_SetCopyrightW(this.midiData, value); }
		}

		/// <summary>
		/// コメントを取得または設定します。
		/// </summary>
		public string Comment
		{
			get
			{
				var comment = new char[256];
				MIDIData_GetCommentW(this.midiData, comment, comment.Length);
				return new string(comment).Trim('\0');
			}
			set { MIDIData_SetCommentW(this.midiData, value); }
		}

		public int TrackCount
		{
			get { return MIDIData_GetNumTrack(this.midiData); }
		}

		/// <summary>
		/// MIDI データの開始時間を取得します。
		/// </summary>
		public int BeginTime
		{
			get { return MIDIData_GetBeginTime(this.midiData); }
		}

		/// <summary>
		/// MIDI データの終了時間を取得します。
		/// </summary>
		public int EndTime
		{
			get { return MIDIData_GetEndTime(this.midiData); }
		}

		/// <summary>
		/// この MIDI データに含まれるトラックを列挙します。
		/// </summary>
		public IEnumerable<MidiTrack> Tracks
		{
			get
			{
				var tracks = new List<MidiTrack>();
				var track = MidiTrack.Create(MIDIData_GetFirstTrack(this.midiData));
				MidiTrack next;

				tracks.Add(track);
				while ((next = track.NextTrack) != null)
				{
					tracks.Add(next);
					track = next;
				}

				return tracks;
			}
		}

		#endregion

		/// <summary>
		/// 新しい MIDI データを作成します。
		/// </summary>
		/// <param name="format">MIDI データ フォーマット</param>
		/// <param name="tracks">最初に用意するトラック数</param>
		/// <param name="resolution">分解能。通常は 48, 72, 96, 120, 144, 192, 240, 360, 384, 480, 960 のどれかを選択します。</param>
		/// <param name="timeMode">タイム モード。通常は <seealso cref="OpenMidiProject.Data.TimeMode.TpqnBase"/> を選択します。</param>
		/// <returns></returns>
		public static MidiData CreateNew(MidiDataFormat format, int tracks, TimeMode timeMode, int resolution)
		{
			var data = MIDIData_Create((int)format, tracks, (int)timeMode, resolution);

			if (data == IntPtr.Zero)
				throw new InvalidOperationException("MIDI データの作成に失敗しました。");

			var me = new MidiData(data);
			return me;
		}

		public static MidiData LoadFile(string filePath)
		{
			var data = IntPtr.Zero;

			var ext = Path.GetExtension(filePath);
			switch (ext)
			{
				case ".mid":
					data = MIDIData_LoadFromSMFW(filePath);
					if (data == IntPtr.Zero)
						throw new FileLoadException("MIDI データの読み込みに失敗しました。");
					return new MidiData(data);
				case ".chy":
					return new MidiData(IntPtr.Zero);
				case ".csv":
					return new MidiData(IntPtr.Zero);
				default:
					throw new FileLoadException("指定されたファイルの拡張子では読み込むことができません。");
			}
		}

		private MidiData(IntPtr midiData)
		{
			this.midiData = midiData;
		}

		public void SaveFile(string filePath)
		{
			var ext = Path.GetExtension(filePath);
			switch (ext)
			{
				case ".mid":
					if(MIDIData_SaveAsSMFW(this.midiData, filePath)==0)
						throw new InvalidOperationException("MIDI データの書き込みに失敗しました。");
					break;
				default:
					throw new FileLoadException("指定されたファイルの拡張子では読み込むことができません。");
			}
		}

		/// <summary>
		/// 指定したイベントが結合できる場合、結合します。
		/// </summary>
		/// <param name="eventType"></param>
		public void CombineEvent(MidiEventType eventType)
		{
			var track = MidiTrack.Create(MIDIData_GetFirstTrack(this.midiData));

			do
			{
				track.CombineEvent(eventType);
			} while ((track = track.NextTrack) != null);
		}

		/// <summary>
		/// 新しい MIDI トラックを追加します。
		/// </summary>
		/// <param name="track"></param>
		public void AddTrack(MidiTrack track)
		{
			if (MIDIData_AddTrack(this.midiData, track.Pointer) == 0)
				throw new InvalidOperationException("MIDI トラックの追加に失敗しました。");
		}

		/// <summary>
		/// MIDI イベントを追加します。
		/// </summary>
		public void AddRangeTrack(IEnumerable<MidiTrack> track)
		{
			foreach (var midiTrack in track)
			{
				AddTrack(midiTrack);
			}
		}

		/// <summary>
		/// 指定した MIDI トラックを削除します。
		/// </summary>
		/// <param name="track"></param>
		public void RemoveTrack(MidiTrack track)
		{
			if (MIDIData_RemoveTrack(this.midiData, track.Pointer) == 0)
				throw new InvalidOperationException("MIDI トラックの削除に失敗しました。");
		}

		/// <summary>
		/// MIDI トラックをすべて削除します。
		/// </summary>
		public void RemoveAllTrack()
		{
			foreach (var track in this.Tracks)
				RemoveTrack(track);
		}

		public MidiTime GetTime(int totalTick)
		{
			var measure = 0;
			var beat = 0;
			var tick = 0;

			MIDIData_BreakTime(this.midiData, totalTick, out measure, out beat, out tick);
			return new MidiTime {Measure = measure, Beat = beat, Tick = tick};
		}

		public void Close()
		{
			MIDIData_Delete(this.midiData);
			this.midiData = IntPtr.Zero;
		}

		public void Dispose()
		{
			Close();
		}

		public MidiDataStructure ToStructure()
		{
			return (MidiDataStructure)Marshal.PtrToStructure(this.midiData, typeof(MidiDataStructure));
		}
	}
}
