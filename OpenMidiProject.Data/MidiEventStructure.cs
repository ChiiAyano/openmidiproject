﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.Data
{
	[StructLayout(LayoutKind.Sequential)]
	public struct MidiEventStructure
	{
		public int m_lTempIndex;
		public int m_lTime;
		public int m_lKind;
		public int m_lLen;
		public IntPtr m_pData;
		public uint m_lData;
		// 以下の IntPtr はすべてこの構造体を指します。
		public IntPtr m_pNextEvent;
		public IntPtr m_pPrevEvent;
		public IntPtr m_pNextSameKindEvent;
		public IntPtr m_pPrevSameKindEvent;
		public IntPtr m_pNextCombinedEvent;
		public IntPtr m_pPrevCombinedEvent;

		public IntPtr m_pParent;
		public int m_lUser1;
		public int m_lUser2;
		public int m_lUser3;
		public int m_lUserFlag;
	}
}
