﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.Clock
{
	[StructLayout(LayoutKind.Sequential)]
    public struct MidiClockStructure
	{
		public int m_lTimeMode;
		public int m_lResolution;
		public int m_lTempo;
		public int m_lDummyTempo;
		public int m_lSpeed;
		public int m_lMIDIInSyncMode;
		public int m_lPeriod;
		public int m_lMillisec;
		public int m_lMillisedMod;
		public int m_lOldMillisec;
		public int m_lOldMillisecMod;
		public int m_lDummyMillisec;
		public int m_lDummyMillisecMod;
		public int m_lOldDummyMillisec;
		public int m_lTickCount;
		public int m_lTickCountMod;
		public int m_lTimerId;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)] public byte[] m_bySMTPE;
		public volatile int m_lRunning;
		public volatile int m_lLocked;
	}
}
