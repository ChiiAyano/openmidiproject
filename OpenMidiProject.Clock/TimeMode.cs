﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.Clock
{
	/// <summary>
	/// タイム モードを定義します。
	/// </summary>
	public enum TimeMode
	{
		/// <summary>
		/// TPQN ベース
		/// </summary>
		TpqnBase = 0,
		/// <summary>
		/// SMPTE 24 ベース (24 フレーム/秒)
		/// </summary>
		Smpte24Base = 24,
		/// <summary>
		/// SMPTE 25 ベース (25 フレーム/秒)
		/// </summary>
		Smpte25Base = 25,
		/// <summary>
		/// SMPTE 29 ベース (29.97 フレーム/秒)
		/// </summary>
		Smpte29Base = 29,
		/// <summary>
		/// SMPTE 30 ベース (30 フレーム/秒)
		/// </summary>
		Smpte30Base = 30
	}
}
