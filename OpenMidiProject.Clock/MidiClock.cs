﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.Clock
{
	public class MidiClock : IDisposable
	{
		#region API 定義

		[DllImport("MIDIClock.dll")]
		private static extern void MIDIClock_Delete(IntPtr pMidiClock);

		[DllImport("MIDIClock.dll")]
		private static extern IntPtr MIDIClock_Create(int lTimeMode, int lResolution, int lTempo);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_GetTimeBase(IntPtr pMidiClock, out int pTimeMode, out int pResolution);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_SetTimeBase(IntPtr pMidiClock, int lTimeMode, int lResolution);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_GetTempo(IntPtr pMidiClock);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_SetTempo(IntPtr pMidiClock, int lTempo);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_GetSpeed(IntPtr pMidiClock);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_SetSpeed(IntPtr pMidiClock, int lSpeed);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_GetMIDIInSyncMode(IntPtr pMidiClock);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_SetMIDIInSyncMode(IntPtr pMidiClock, int lMidiInSyncMode);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_Start(IntPtr pMidiClock);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_Stop(IntPtr pMidiClock);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_Reset(IntPtr pMidiClock);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_IsRunning(IntPtr pMidiClock);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_GetMillisec(IntPtr pMidiClock);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_SetMillisec(IntPtr pMidiClock, int lMillisec);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_GetTickCount(IntPtr pMidiClock);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_SetTickCount(IntPtr pMidiClock, int lTickCount);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_PutMIDITimingClock(IntPtr pMidiClock);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_PutSysExSMPTEMTC(IntPtr pMIdiClock, byte cHour, byte cMinute, byte cSecond,
		                                                     byte cFrame);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_PutSMPTEMTC(IntPtr pMidiClock, byte cSmptemtc);

		[DllImport("MIDIClock.dll")]
		private static extern int MIDIClock_PutMIDIMessage(IntPtr pMidiClock, byte[] pMessage, int lLen);

		#endregion

		private IntPtr midiClock = IntPtr.Zero;

		#region 静的プロパティ

		/// <summary>
		/// 設定できる最大のテンポを取得します。
		/// </summary>
		public static int MaxTempo
		{
			get { return 60000000; }
		}

		/// <summary>
		/// 設定できる最小のテンポを取得します。
		/// </summary>
		public static int MinTempo
		{
			get { return 1; }
		}

		/// <summary>
		/// 通常の再生速度の値を取得します。
		/// </summary>
		public static int NormalSpeed
		{
			get { return 10000; }
		}

		/// <summary>
		/// 遅い再生速度の値を取得します。
		/// </summary>
		public static int SlowSpeed
		{
			get { return 5000; }
		}

		/// <summary>
		/// 速い再生速度の値を取得します。
		/// </summary>
		public static int FastSpeed
		{
			get { return 20000; }
		}

		/// <summary>
		/// 設定できる最小の再生速度を取得します。
		/// </summary>
		public static int MinSpeed
		{
			get { return 0; }
		}
		
		/// <summary>
		/// 設定できる最大の再生速度を取得します。
		/// </summary>
		public static int MaxSpeed
		{
			get { return 100000; }
		}

		#endregion

		#region プロパティ

		/// <summary>
		/// MIDI クロックが動作中かどうかを取得します。
		/// </summary>
		public bool IsRunning
		{
			get
			{
				if (this.midiClock == IntPtr.Zero) return false;
				return MIDIClock_IsRunning(this.midiClock) != 0;
			}
		}

		/// <summary>
		/// 現在のテンポを取得または設定します。
		/// </summary>
		public double Tempo
		{
			get
			{
				if (this.midiClock == IntPtr.Zero) return 0;

				return (double)MidiClock.MaxTempo / MIDIClock_GetTempo(this.midiClock);
			}
			set
			{
				if (this.midiClock == IntPtr.Zero) throw new InvalidOperationException("MIDI クロックが生成されていないか無効です。");

				if (value < MidiClock.MinTempo || MidiClock.MaxTempo < value)
					throw new InvalidOperationException(
						"テンポは " + MidiClock.MinTempo + " から " + MidiClock.MaxTempo + " の範囲内である必要があります。");

				if (MIDIClock_SetTempo(this.midiClock, (int)(MidiClock.MaxTempo / value)) == 0)
					throw new InvalidOperationException("テンポの設定に失敗しました。");
			}
		}

		/// <summary>
		/// 現在の再生速度を取得または設定します。
		/// </summary>
		public int Speed
		{
			get
			{
				if (this.midiClock == IntPtr.Zero) return 0;

				return MIDIClock_GetSpeed(this.midiClock);
			}
			set
			{
				if (this.midiClock == IntPtr.Zero) throw new InvalidOperationException("MIDI クロックが生成されていないか無効です。");

				if (value < MidiClock.MinSpeed || MidiClock.MaxSpeed < value)
					throw new InvalidOperationException(
						"再生速度は " + MidiClock.MinSpeed + " から " + MidiClock.MaxSpeed + " の範囲内である必要があります。");

				if (MIDIClock_SetSpeed(this.midiClock, value) == 0)
					throw new InvalidOperationException("再生速度の設定に失敗しました。");
			}
		}

		/// <summary>
		/// 現在の分解能を取得します。
		/// </summary>
		public int TimeBase
		{
			get
			{
				if (this.midiClock == IntPtr.Zero) return 0;

				int timeMode;
				int resolution;

				MIDIClock_GetTimeBase(this.midiClock, out timeMode, out resolution);

				return resolution;
			}
		}

		/// <summary>
		/// 現在のタイム モードを取得します。
		/// </summary>
		public TimeMode TimeMode
		{
			get
			{
				if (this.midiClock == IntPtr.Zero) return TimeMode.TpqnBase;

				int timeMode;
				int resolution;

				MIDIClock_GetTimeBase(this.midiClock, out timeMode, out resolution);

				return (TimeMode)timeMode;
			}
		}

		/// <summary>
		/// 現在の MIDI 入力同期モードを取得または設定します。
		/// </summary>
		public SyncMode? SyncMode
		{
			get
			{
				if (this.midiClock == IntPtr.Zero) return null;
				return (SyncMode)MIDIClock_GetMIDIInSyncMode(this.midiClock);
			}
			set
			{
				if (this.midiClock == IntPtr.Zero) throw new InvalidOperationException("MIDI クロックが生成されていないか無効です。");

				if (value != null && MIDIClock_SetMIDIInSyncMode(this.midiClock, (int)value) == 0)
					throw new InvalidOperationException("再生速度の設定に失敗しました。");
			}
		}

		/// <summary>
		/// 現在の経過時間を取得します。
		/// </summary>
		public TimeSpan ElapsedTime
		{
			get
			{
				if (this.midiClock == IntPtr.Zero) return TimeSpan.Zero;
				return TimeSpan.FromMilliseconds(MIDIClock_GetMillisec(this.midiClock));
			}
		}

		/// <summary>
		/// 現在の経過時間を取得します。
		/// </summary>
		public int ElapsedTick
		{
			get
			{
				if (this.midiClock == IntPtr.Zero) return 0;
				return MIDIClock_GetTickCount(this.midiClock);
			}
		}

		#endregion

		/// <summary>
		/// <see cref="OpenMidiProject.Clock.MidiClock"/> クラスの新しいインスタンスを生成します。
		/// </summary>
		/// <param name="tempo">最初に割り当てるテンポ</param>
		/// <param name="resolution">分解能。通常は 48, 72, 96, 120, 144, 192, 240, 360, 384, 480, 960 のどれかを選択します。</param>
		/// <param name="timeMode">タイム モード。通常は <seealso cref="OpenMidiProject.Clock.TimeMode.TpqnBase"/> を選択します。</param>
		public MidiClock(double tempo, int resolution, TimeMode timeMode)
		{
			this.midiClock = MIDIClock_Create((int)timeMode, resolution, (int)(MidiClock.MaxTempo / tempo));

			if (this.midiClock == IntPtr.Zero)
				throw new InvalidOperationException("MIDI クロックの生成に失敗しました。");
		}

		/// <summary>
		/// <see cref="OpenMidiProject.Clock.MidiClock"/> クラスの新しいインスタンスを生成します。
		/// </summary>
		/// <param name="tempo">最初に割り当てるテンポ</param>
		/// <param name="resolution">分解能。通常は 48, 72, 96, 120, 144, 192, 240, 360, 384, 480, 960 のどれかを選択します。</param>
		public MidiClock(double tempo, int resolution)
			: this(tempo, resolution, TimeMode.TpqnBase)
		{
		}

		/// <summary>
		/// <see cref="OpenMidiProject.Clock.MidiClock"/> クラスの新しいインスタンスを生成します。
		/// </summary>
		/// <param name="tempo">最初に割り当てるテンポ</param>
		public MidiClock(double tempo)
			: this(tempo, 480, TimeMode.TpqnBase)
		{
		}

		/// <summary>
		/// MIDI クロックをスタートします。
		/// </summary>
		/// <param name="reset">経過時間をリセットしてから開始するか</param>
		public void Start(bool reset)
		{
			if (this.midiClock == IntPtr.Zero)
				throw new InvalidOperationException("MIDI クロックが生成されていないか無効です。");

			if (reset)
			{
				if (this.IsRunning)
					Stop();

				Reset();
			}

			if (MIDIClock_Start(this.midiClock) == 0)
				throw new InvalidOperationException("クロックのスタートに失敗しました。");
		}

		/// <summary>
		/// MIDI クロックを停止します。
		/// </summary>
		public void Stop()
		{
			if (this.midiClock == IntPtr.Zero)
				throw new InvalidOperationException("MIDI クロックが生成されていないか無効です。");

			if (!this.IsRunning)
				return;

			if (MIDIClock_Stop(this.midiClock) == 0)
				throw new InvalidOperationException("クロックの停止に失敗しました。");
		}

		/// <summary>
		/// MIDI クロックの経過時間をリセットします。
		/// </summary>
		public void Reset()
		{
			if (this.midiClock == IntPtr.Zero)
				throw new InvalidOperationException("MIDI クロックが生成されていないか無効です。");

			if (MIDIClock_Reset(this.midiClock) == 0)
				throw new InvalidOperationException("クロックのリセットに失敗しました。");
		}

		/// <summary>
		/// 指定された時間に MIDI クロックの経過時間を変更します。
		/// </summary>
		/// <param name="time"></param>
		public void Seek(TimeSpan time)
		{
			// TODO: この部分は中途半端なのできっちり実装すること。Tickも同時に設定することとドキュメントにあるため。

			if (this.midiClock == IntPtr.Zero)
				throw new InvalidOperationException("MIDI クロックが生成されていないか無効です。");

			if (MIDIClock_SetMillisec(this.midiClock, (int)time.TotalMilliseconds) == 0)
				throw new InvalidOperationException("経過時間の設定に失敗しました。");
		}

		/// <summary>
		/// 指定された Tick に MIDI クロックの経過時間を変更します。
		/// </summary>
		/// <param name="tick"></param>
		public void Seek(int tick)
		{
			// TODO: この部分は中途半端なのできっちり実装すること。Millisecondも同時に設定することとドキュメントにあるため。

			if (this.midiClock == IntPtr.Zero)
				throw new InvalidOperationException("MIDI クロックが生成されていないか無効です。");

			if (MIDIClock_SetTickCount(this.midiClock, tick) == 0)
				throw new InvalidOperationException("経過時間の設定に失敗しました。");
		}

		/// <summary>
		/// MIDI クロックに MIDI メッセージを送信します。
		/// </summary>
		/// <param name="message"></param>
		public void SendMessage(byte[] message)
		{
			if (this.midiClock == IntPtr.Zero)
				throw new InvalidOperationException("MIDI クロックが生成されていないか無効です。");

			if (MIDIClock_PutMIDIMessage(this.midiClock, message, message.Length) == 0)
				throw new InvalidOperationException("MIDI メッセージの送信に失敗しました。");
		}

		/// <summary>
		/// 分解能を設定します。
		/// </summary>
		/// <param name="resolution">分解能。通常は 48, 72, 96, 120, 144, 192, 240, 360, 384, 480, 960 のどれかを選択します。</param>
		/// <param name="timeMode">タイム モード。通常は <seealso cref="OpenMidiProject.Clock.TimeMode.TpqnBase"/> を選択します。</param>
		public void SetTimeBase(int resolution, TimeMode timeMode)
		{
			if (this.midiClock == IntPtr.Zero) throw new InvalidOperationException("MIDI クロックが生成されていないか無効です。");

			if (MIDIClock_SetTimeBase(this.midiClock, (int)timeMode, resolution) == 0)
				throw new InvalidOperationException("分解能の設定に失敗しました。");
		}

		/// <summary>
		/// 分解能を設定します。
		/// </summary>
		/// <param name="resolution">分解能。通常は 48, 72, 96, 120, 144, 192, 240, 360, 384, 480, 960 のどれかを選択します。</param>
		public void SetTimeBase(int resolution)
		{
			SetTimeBase(resolution, this.TimeMode);
		}


		public void Close()
		{
			if (this.midiClock == IntPtr.Zero)
				return;

			MIDIClock_Delete(this.midiClock);
			this.midiClock = IntPtr.Zero;
		}

		public void Dispose()
		{
			Close();
		}

		public MidiClockStructure ToStructure()
		{
			return (MidiClockStructure)Marshal.PtrToStructure(this.midiClock, typeof(MidiClockStructure));
		}
	}
}
