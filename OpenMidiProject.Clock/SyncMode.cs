﻿namespace OpenMidiProject.Clock
{
	/// <summary>
	/// MIDI 入力同期モードを定義します。
	/// </summary>
	public enum SyncMode
	{
		/// <summary>
		/// マスター モード
		/// </summary>
		Master = 0,
		/// <summary>
		/// スレーブ モード (MIDI タイミング クロック追従)
		/// </summary>
		TimingClock = 1,
		/// <summary>
		/// スレーブ モード (SMPTE/MTC 追従)
		/// </summary>
		SmpteMtc = 2
	}
}