﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.IO
{
	public class MidiOutMessageSendEventArgs : EventArgs
	{
		/// <summary>
		/// 実際に送信した MIDI メッセージを取得します。
		/// </summary>
		public byte[] Message { get; private set; }

		public MidiOutMessageSendEventArgs(byte[] message)
		{
			this.Message = message;
		}
	}
}
