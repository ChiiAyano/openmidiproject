﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.IO
{

	public class MidiInReceiveMessageEventArgs : EventArgs
	{
		/// <summary>
		/// 取得した MIDI メッセージを取得します。
		/// </summary>
		public byte[] Message { get; private set; }

		public MidiInReceiveMessageEventArgs(byte[] message)
		{
			this.Message = message;
		}
	}
}
