﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.IO
{
    public class MidiOut : IDisposable
    {
		private IntPtr midiDevice;
	    private string deviceName = string.Empty;

		#region API 定義

	    [DllImport("MIDIIO.dll")]
	    private static extern int MIDIOut_GetDeviceNum();

	    [DllImport("MIDIIO.dll", CharSet = CharSet.Unicode)]
	    private static extern int MIDIOut_GetDeviceNameW(int lId, char[] pszDeviceName, int lLen);

		[DllImport("MIDIIO.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIOut_OpenW(string pszDeviceName);

	    [DllImport("MIDIIO.dll")]
		private static extern int MIDIOut_Close(IntPtr pMidiDevice);

		[DllImport("MIDIIO.dll", CharSet = CharSet.Unicode)]
	    private static extern IntPtr MIDIOut_ReopenW(IntPtr pMidiOut, string pszDeviceName);

	    [DllImport("MIDIIO.dll")]
		private static extern int MIDIOut_Reset(IntPtr pMidiDevice);

	    [DllImport("MIDIIO.dll")]
		private static extern int MIDIOut_PutMIDIMessage(IntPtr pMidi, byte[] pMessage, int lLen);

	    [DllImport("MIDIIO.dll")]
		private static extern int MIDIOut_PutByte(IntPtr pMidi, byte cByte);

	    [DllImport("MIDIIO.dll")]
		private static extern int MIDIOut_PutBytes(IntPtr pMidi, byte[] pBuf, int lLen);

		[DllImport("MIDIIO.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDIOut_GetThisDeviceNameW(ref IntPtr pMidiOut, char[] pszDeviceName, int lLen);

	    #endregion

		#region 静的プロパティ

		/// <summary>
		/// MIDI 出力デバイスの数を取得します。
		/// </summary>
	    public static int DeviceCount
	    {
		    get { return MIDIOut_GetDeviceNum(); }
	    }

		/// <summary>
		/// MIDI 出力デバイスの名前を列挙します。
		/// </summary>
	    public static IEnumerable<string> DeviceNames
	    {
		    get
		    {
			    var devices = new List<string>();

			    for (int i = 0; i < DeviceCount; i++)
			    {
				    var nameCh = new char[256];
				    var result = MIDIOut_GetDeviceNameW(i, nameCh, 256);
					var name = new string(nameCh).Trim('\0');

				    devices.Add(name);
			    }

			    return devices;
		    }
	    }

		#endregion

		#region 静的メソッド

		/// <summary>
		/// デバイス名から <see cref="OpenMidiProject.IO.MidiOut"/> クラスのインスタンスを生成します。
		/// </summary>
		/// <param name="deviceName">使用する MIDI 出力デバイス名</param>
		/// <returns></returns>
	    public static MidiOut Create(string deviceName)
	    {
		    return new MidiOut(deviceName);
	    }

		/// <summary>
		/// デバイス番号から <see cref="OpenMidiProject.IO.MidiOut"/> クラスのインスタンスを生成します。
		/// </summary>
		/// <param name="deviceNumber">使用する MIDI 出力デバイスの番号</param>
		/// <returns></returns>
	    public static MidiOut Create(int deviceNumber)
	    {
		    return new MidiOut(deviceNumber);
	    }

	    #endregion

		#region プロパティ

		/// <summary>
		/// 現在開いている MIDI 出力デバイスの名前を取得します。
		/// </summary>
	    public string DeviceName
	    {
		    get
		    {
			    var nameCh = new char[256];
			    MIDIOut_GetThisDeviceNameW(ref this.midiDevice, nameCh, nameCh.Length);
			    var name = new string(nameCh).Trim('\0');

			    return name;
		    }
	    }

	    #endregion

	    public event EventHandler<MidiOutMessageSendEventArgs> MessageSend;

		/// <summary>
		/// <see cref="OpenMidiProject.IO.MidiOut"/> クラスの新しいインスタンスを生成します。
		/// </summary>
		/// <param name="deviceName">使用する MIDI 出力デバイスの名前</param>
	    public MidiOut(string deviceName)
	    {
		    this.midiDevice = MIDIOut_OpenW(deviceName);

			if (this.midiDevice == IntPtr.Zero)
				throw new InvalidOperationException("MIDI 出力デバイスをオープンできません。");

			this.deviceName = deviceName;

			// var marshal = (MidiStructure)Marshal.PtrToStructure(this.midiDevice, typeof(MidiStructure));
	    }

		/// <summary>
		/// <see cref="OpenMidiProject.IO.MidiOut"/> クラスの新しいインスタンスを生成します。
		/// </summary>
		/// <param name="deviceNumber">使用する MIDI 出力デバイスのインデックス</param>
	    public MidiOut(int deviceNumber)
		    : this(DeviceNames.Skip(deviceNumber).FirstOrDefault())
	    {
	    }

		/// <summary>
		/// MIDI 出力デバイスの状態をリセットします。
		/// </summary>
	    public void Reset()
	    {
		    if (MIDIOut_Reset(this.midiDevice) == 0)
			    throw new InvalidOperationException("デバイスにアクセスできません。");
	    }

		/// <summary>
		/// MIDI 出力デバイスへメッセージを送信します。
		/// </summary>
		/// <param name="message"></param>
	    public void SendMessage(byte[] message)
		{
			var result = MIDIOut_PutMIDIMessage(this.midiDevice, message, message.Length);
			Console.WriteLine("{0} -> {1} ({2})", this.deviceName, string.Join(" ", message.Select(s => "0x" + s.ToString("x"))),
			                  result);

			if (this.MessageSend != null)
				this.MessageSend(this, new MidiOutMessageSendEventArgs(message));
		}

		/// <summary>
		/// MIDI 出力デバイスを閉じます。
		/// </summary>
	    public void Close()
	    {
		    MIDIOut_Close(this.midiDevice);
			this.midiDevice = IntPtr.Zero;
	    }

	    public void Dispose()
	    {
		    Close();
		}

		#region MIDI メッセージ簡易送信

		/// <summary>
		/// 指定されたチャンネルのノート番号をオンにします。
		/// </summary>
		/// <param name="channel">チャンネル番号。トラックともいい、0 ～ 15 まで指定できます。</param>
		/// <param name="note">ノート番号。中央のドを 60 とし 0 ～ 127 まで指定できます。</param>
		/// <param name="velocity">ベロシティ (音の強さ)。0 を指定するとノート オフと同様の動作になります。0 ～ 127 まで指定できます。</param>
		public void NoteOn(int channel, int note, int velocity)
		{
			if (channel > 15)
				throw new ArgumentOutOfRangeException("channel", "channel で指定できるのは 0 ～ 15 の範囲までです。");
			if (note > 127)
				throw new ArgumentOutOfRangeException("note", "note で指定できるのは 0 ～ 127 の範囲までです。");
			if (velocity > 127)
				throw new ArgumentOutOfRangeException("velocity", "velocity で指定できるのは 0 ～ 127 の範囲までです。");

			SendMessage(new[] { ((byte)(0x90 + channel)), (byte)note, (byte)velocity });
		}

	    /// <summary>
	    /// 指定されたチャンネルのノート番号をオフにします。
	    /// </summary>
	    /// <param name="channel">チャンネル番号。トラックともいい、0 ～ 15 まで指定できます。</param>
	    /// <param name="note">ノート番号。中央のドを 60 とし 0 ～ 127 まで指定できます。</param>
	    /// <param name="velocity">ベロシティ (音の強さ)。0 を指定するとノート オフと同様の動作になります。0 ～ 127 まで指定できます。</param>
	    public void NoteOff(int channel, int note, int velocity)
	    {
		    if (channel > 15)
			    throw new ArgumentOutOfRangeException("channel", "channel で指定できるのは 0 ～ 15 の範囲までです。");
		    if (note > 127)
			    throw new ArgumentOutOfRangeException("note", "note で指定できるのは 0 ～ 127 の範囲までです。");
		    if (velocity > 127)
			    throw new ArgumentOutOfRangeException("velocity", "velocity で指定できるのは 0 ～ 127 の範囲までです。");

		    SendMessage(new[] { ((byte)(0x80 + channel)), (byte)note, (byte)velocity });
	    }

	    /// <summary>
	    /// 音色を変更します。
	    /// </summary>
	    /// <param name="channel">チャンネル番号。トラックともいい、0 ～ 15 まで指定できます。</param>
	    /// <param name="pc">プログラム チェンジ番号。0 ～ 127 まで指定できます。</param>
	    public void ProgramChange(int channel, int pc)
	    {
		    if (channel > 15)
			    throw new ArgumentOutOfRangeException("channel", "channel で指定できるのは 0 ～ 15 の範囲までです。");
		    if (pc > 127)
			    throw new ArgumentOutOfRangeException("pc", "note で指定できるのは 0 ～ 127 の範囲までです。");

		    SendMessage(new[] { (byte)(0xC0 + channel), (byte)pc });
	    }

	    #endregion
	}
}
