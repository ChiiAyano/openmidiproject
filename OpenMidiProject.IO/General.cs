﻿namespace OpenMidiProject.IO
{
	public static class General
	{
		public const int BufferSize = 4096;
		public const int SystemExclusiveNumber = 4;
		public const int SystemExclusiveSize = 1024;
		public const int ModeIn = 0x0000;
		public const int ModeOut = 0x0001;

		public static readonly string NoneName = "(None)";
		public static readonly string NoneNameJ = "(なし)";
		public static readonly string Default = "Default";
		public static readonly string DefaultJ = "デフォルト";
		public static readonly string MidiMapper = "MIDI Mapper";
		public static readonly string MidiMapperJ = "MIDI マッパー";
	}
}