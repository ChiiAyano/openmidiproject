﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.IO
{
	[StructLayout(LayoutKind.Sequential)]
	public struct MidiStructure
	{
		public IntPtr m_pDeviceHandle;
		public IntPtr m_pDeviceName;
		public int m_lMode;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = General.SystemExclusiveNumber)] public IntPtr[] m_pSysxHeader;
		public int m_bStarting;
		public byte[] m_pBuf;
		public int m_lBufSize;
		public int m_lReadPosition;
		public int m_lWritePosition;
		public int m_bBufLocked;
		public byte m_byRunningStatus;
	}
}
