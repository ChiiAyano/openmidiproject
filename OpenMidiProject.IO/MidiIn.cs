﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.IO
{
	public class MidiIn : IDisposable
	{
		private IntPtr midiDevice;
		private string deviceName = string.Empty;

		#region API 定義

		[DllImport("MIDIIO.dll")]
		private static extern int MIDIIn_GetDeviceNum();

		[DllImport("MIDIIO.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDIIn_GetDeviceNameW(int lId, char[] pszDeviceName, int lLen);

		[DllImport("MIDIIO.dll", CharSet = CharSet.Unicode)]
		private static extern IntPtr MIDIIn_OpenW(string pszDeviceName);

		[DllImport("MIDIIO.dll")]
		private static extern int MIDIIn_Close(IntPtr pMidiDevice);

		[DllImport("MIDIIO.dll")]
		private static extern IntPtr MIDIIn_ReopenW(IntPtr pMidiIn, char[] pszDeviceName);

		[DllImport("MIDIIO.dll")]
		private static extern int MIDIIn_Reset(IntPtr pMidiDevice);

		[DllImport("MIDIIO.dll")]
		private static extern int MIDIIn_GetMIDIMessage(IntPtr pMidiIn, byte[] pMessage, int lLen);

		[DllImport("MIDIIO.dll", CharSet = CharSet.Unicode)]
		private static extern int MIDIIn_GetThisDeviceNameW(IntPtr pMidiIn, char[] pszDeviceName, int lLen);

		#endregion

		#region 静的プロパティ

		/// <summary>
		/// MIDI 出力デバイスの数を取得します。
		/// </summary>
		public static int DeviceCount
		{
			get { return MIDIIn_GetDeviceNum(); }
		}

		/// <summary>
		/// MIDI 入力デバイスの名前を列挙します。
		/// </summary>
		public static IEnumerable<string> DeviceNames
		{
			get
			{
				var devices = new List<string>();

				for (int i = 0; i < DeviceCount; i++)
				{
					var nameCh = new char[256];
					var result = MIDIIn_GetDeviceNameW(i, nameCh, 256);
					var name = new string(nameCh).Trim('\0');

					devices.Add(name);
				}

				return devices;
			}
		}

		#endregion

		#region 静的メソッド

		/// <summary>
		/// デバイス名から <see cref="OpenMidiProject.IO.MidiIn"/> クラスのインスタンスを生成します。
		/// </summary>
		/// <param name="deviceName">使用する MIDI 入力デバイス名</param>
		/// <returns></returns>
		public static MidiIn Create(string deviceName)
		{
			return new MidiIn(deviceName);
		}

		/// <summary>
		/// デバイス番号から <see cref="OpenMidiProject.IO.MidiIn"/> クラスのインスタンスを生成します。
		/// </summary>
		/// <param name="deviceNumber">使用する MIDI 入力デバイスの番号</param>
		/// <returns></returns>
		public static MidiIn Create(int deviceNumber)
		{
			return new MidiIn(deviceNumber);
		}

		#endregion

		#region プロパティ

		/// <summary>
		/// 現在開いている MIDI 入力デバイスの名前を取得します。
		/// </summary>
		public string DeviceName
		{
			get
			{
				var nameCh = new char[256];
				MIDIIn_GetThisDeviceNameW(this.midiDevice, nameCh, nameCh.Length);
				var name = new string(nameCh).Trim('\0');

				return name;
			}
		}

		#endregion

		public event EventHandler<MidiInReceiveMessageEventArgs> ReceiveMessage;

		/// <summary>
		/// <see cref="OpenMidiProject.IO.MidiIn"/> クラスの新しいインスタンスを生成します。
		/// </summary>
		/// <param name="deviceName">使用する MIDI 入力デバイスの名前</param>
		public MidiIn(string deviceName)
		{
			this.midiDevice = MIDIIn_OpenW(deviceName);

			if (this.midiDevice == IntPtr.Zero)
				throw new InvalidOperationException("MIDI 入力デバイスをオープンできません。");

			this.deviceName = deviceName;

			var marshal = (MidiStructure)Marshal.PtrToStructure(this.midiDevice, typeof(MidiStructure));

			// 取得待機状態にしておく
			Task.Factory.StartNew(Receive);
		}

		/// <summary>
		/// <see cref="OpenMidiProject.IO.MidiIn"/> クラスの新しいインスタンスを生成します。
		/// </summary>
		/// <param name="deviceNumber">使用する MIDI 入力デバイスのインデックス</param>
		public MidiIn(int deviceNumber)
			: this(DeviceNames.Skip(deviceNumber).FirstOrDefault())
		{
		}

		/// <summary>
		/// MIDI 入力デバイスの状態をリセットします。
		/// </summary>
		public void Reset()
		{
			if (MIDIIn_Reset(this.midiDevice) == 0)
				throw new InvalidOperationException("デバイスにアクセスできません。");
		}

		/// <summary>
		/// MIDI 入力デバイスを閉じます。
		/// </summary>
		public void Close()
		{
			MIDIIn_Close(this.midiDevice);
			this.midiDevice = IntPtr.Zero;
		}

		public void Dispose()
		{
			Close();
		}

		private void Receive()
		{
			while (this.midiDevice == IntPtr.Zero)
			{
				lock (this)
				{
					var data = new byte[256];
					var length = MIDIIn_GetMIDIMessage(this.midiDevice, data, data.Length);

					if (length <= 0) continue;

					if (this.ReceiveMessage != null)
						this.ReceiveMessage(this, new MidiInReceiveMessageEventArgs(data));
				}
			}
		}
	}
}
