﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenMidiProject.IO;
using OpenMidiProject.Status;
using OpenMidiProject.Clock;
using OpenMidiProject.Data;
using System.Threading;

namespace OpenMidiProject.Midi
{
	/// <summary>
	/// MIDI に関する制御をおこないます。
	/// </summary>
    public class MidiManager : IDisposable
	{
		private List<MidiOut> midiOut;
		private List<MidiIn> midiIn;
		private MidiStatus midiStatus = null;
		private MidiClock midiClock = null;
		private MidiData midiData;

		private readonly double defaultTempo = 120d;

		public event EventHandler<MidiInReceiveMessageEventArgs> MidiInReceiveMessage;
		public event EventHandler<TickEventArgs> Tick;

		#region プロパティ

		/// <summary>
		/// 使用する MIDI 出力デバイスを取得または設定します。
		/// </summary>
		public List<MidiOut> MidiOut
		{
			get { return this.midiOut; }
			set
			{
				this.midiOut = value;
				this.midiStatus = new MidiStatus(ModuleMode.Native);
			}
		}

		/// <summary>
		/// 使用する MIDI 入力デバイスを取得または設定します。
		/// </summary>
		public List<MidiIn> MidiIn
		{
			get { return this.midiIn; }
			set
			{
				this.midiIn = value;
			}
		}

		/// <summary>
		/// 現在のテンポを取得または設定します。
		/// </summary>
		public double Tempo
		{
			get
			{
				if (this.midiClock == null) return 0d;
				return this.midiClock.Tempo;
			}
			set { this.midiClock.Tempo = value; }
		}

		/// <summary>
		/// 分解能を取得または設定します。
		/// </summary>
		public int Timebase
		{
			get
			{
				if (this.midiClock == null) return 0;
				return this.midiClock.TimeBase;
			}
			set { this.midiClock.SetTimeBase(value); }
		}

		/// <summary>
		/// 現在の MIDI デバイスのチャンネル情報を取得します。
		/// </summary>
		public IEnumerable<MidiPart> Channel
		{
			get
			{
				if (this.midiStatus == null) return null;

				return this.midiStatus.Part;
			}
		}

		public double Speed
		{
			get { return this.midiClock.Speed / 10000d; }
			set { this.midiClock.Speed = (int)(value * 10000); }
		}

		public int TotalTick { get; set; }

		public bool IsPlaying { get; private set; }

		/// <summary>
		/// 可能な場合、MIDI ファイルのタイトルを取得します。
		/// </summary>
		public string Title { get; private set; }

		#endregion

		void midiIn_ReceiveMessage(object sender, MidiInReceiveMessageEventArgs e)
		{
			if (this.MidiInReceiveMessage != null)
				this.MidiInReceiveMessage(this, e);
		}

		void midiOut_MessageSend(object sender, MidiOutMessageSendEventArgs e)
		{
			try
			{
				this.midiStatus.SendMessage(e.Message);
			}
			catch(Exception ex)
			{
				Console.WriteLine("Exception: {0}", ex.Message);
			}
		}

		/// <summary>
		/// <see cref="OpenMidiProject.Midi.MidiManager"/> クラスの新しいインスタンスを生成します。
		/// </summary>
		/// <param name="midiOut">使用する MIDI 出力デバイス</param>
		/// <param name="midiIn">使用する MIDI 入力デバイス</param>
		public MidiManager(IEnumerable<MidiOut> midiOut, IEnumerable<MidiIn> midiIn)
		{
			this.midiOut = midiOut == null ? null : midiOut.ToList();
			this.midiIn = midiIn == null ? null : midiIn.ToList();
			this.midiStatus = new MidiStatus(ModuleMode.Native);
			this.midiClock = new MidiClock(this.defaultTempo);

			this.Tempo = 120;
			this.Timebase = 480;

			this.midiData = MidiData.CreateNew(MidiDataFormat.Format1, 1, Data.TimeMode.TpqnBase, this.Timebase);

			// MidiDataのセットアップトラックを設定
			var setup = this.midiData.Tracks.FirstOrDefault();
			if (setup == null)
				throw new InvalidProgramException("セットアップ トラックが生成されていません。");
			setup.OutputChannel = -1;
			setup.AddRangeEvent(new[]
				                    {
					                    MidiEvent.CreateTempo(0, this.Tempo),
					                    MidiEvent.CreateMeter(0, 4, 4, this.Timebase),
					                    MidiEvent.CreateEndOfTrack(20)
				                    });

			if (this.midiIn != null)
				foreach (var mi in this.midiIn)
					mi.ReceiveMessage += midiIn_ReceiveMessage;

			if(this.midiOut!=null)
				foreach(var mo in this.midiOut)
					mo.MessageSend += midiOut_MessageSend;

			// 再生用にClockのイベントを利用
			this.Tick += MidiManager_Tick;
		}

		/// <summary>
		/// <see cref="OpenMidiProject.Midi.MidiManager"/> クラスの新しいインスタンスを生成します。
		/// </summary>
		/// <param name="midiOut">使用する MIDI 出力デバイス</param>
		public MidiManager(IEnumerable<MidiOut> midiOut)
			: this(midiOut, null)
		{
		}

		/// <summary>
		/// <see cref="OpenMidiProject.Midi.MidiManager"/> クラスの新しいインスタンスを生成します。
		/// </summary>
		/// <param name="midiIn">使用する MIDI 入力デバイス</param>
		public MidiManager(IEnumerable<MidiIn> midiIn)
			: this(null, midiIn)
		{
		}

		/// <summary>
		/// <see cref="OpenMidiProject.Midi.MidiManager"/> クラスの新しいインスタンスを生成します。
		/// </summary>
		public MidiManager()
			: this(null, null)
		{
		}

		public void LoadFile(string filePath)
		{
			this.midiData = MidiData.LoadFile(filePath);
			this.Timebase = this.midiData.Resolution;
			this.midiStatus = new MidiStatus(ModuleMode.Native, this.midiData.TrackCount, 1);

			// タイトルを取得できれば取得する
			this.Title = this.midiData.Title;
		}

		#region MIDI Out

		/// <summary>
		/// MIDI 出力デバイスにメッセージを送信します。
		/// </summary>
		/// <param name="message"></param>
		public void SendMessage(byte[] message)
		{
			//this.midiOut.SendMessage(message);
			this.midiStatus.SendMessage(message);
		}

		/// <summary>
		/// 指定されたチャンネルのノート番号をオンにします。
		/// </summary>
		/// <param name="channel">チャンネル番号。トラックともいい、0 ～ 15 まで指定できます。</param>
		/// <param name="note">ノート番号。中央のドを 60 とし 0 ～ 127 まで指定できます。</param>
		/// <param name="velocity">ベロシティ (音の強さ)。0 を指定するとノート オフと同様の動作になります。0 ～ 127 まで指定できます。</param>
		public void NoteOn(byte channel, byte note, byte velocity)
		{
			if (channel > 15)
				throw new ArgumentOutOfRangeException("channel", "channel で指定できるのは 0 ～ 15 の範囲までです。");
			if (note > 127)
				throw new ArgumentOutOfRangeException("note", "note で指定できるのは 0 ～ 127 の範囲までです。");
			if (velocity > 127)
				throw new ArgumentOutOfRangeException("velocity", "velocity で指定できるのは 0 ～ 127 の範囲までです。");

			SendMessage(new[] { ((byte)(0x90 + channel)), note, velocity });
		}

		/// <summary>
		/// 指定されたチャンネルのノート番号をオフにします。
		/// </summary>
		/// <param name="channel">チャンネル番号。トラックともいい、0 ～ 15 まで指定できます。</param>
		/// <param name="note">ノート番号。中央のドを 60 とし 0 ～ 127 まで指定できます。</param>
		/// <param name="velocity">ベロシティ (音の強さ)。0 を指定するとノート オフと同様の動作になります。0 ～ 127 まで指定できます。</param>
		public void NoteOff(byte channel, byte note, byte velocity)
		{
			if (channel > 15)
				throw new ArgumentOutOfRangeException("channel", "channel で指定できるのは 0 ～ 15 の範囲までです。");
			if (note > 127)
				throw new ArgumentOutOfRangeException("note", "note で指定できるのは 0 ～ 127 の範囲までです。");
			if (velocity > 127)
				throw new ArgumentOutOfRangeException("velocity", "velocity で指定できるのは 0 ～ 127 の範囲までです。");

			SendMessage(new[] { ((byte)(0x80 + channel)), note, velocity });
		}

		/// <summary>
		/// 音色を変更します。
		/// </summary>
		/// <param name="channel">チャンネル番号。トラックともいい、0 ～ 15 まで指定できます。</param>
		/// <param name="pc">プログラム チェンジ番号。0 ～ 127 まで指定できます。</param>
		/// <param name="msb">バンクセレクトの MSB。0 ～ 127 まで指定できます。</param>
		/// <param name="lsb">バンクセレクトの LSB。0 ～ 127 まで指定できます。</param>
		public void ProgramChange(byte channel, byte pc, byte msb, byte lsb)
		{
			if (channel > 15)
				throw new ArgumentOutOfRangeException("channel", "channel で指定できるのは 0 ～ 15 の範囲までです。");
			if (pc > 127)
				throw new ArgumentOutOfRangeException("pc", "note で指定できるのは 0 ～ 127 の範囲までです。");
			if (msb > 127)
				throw new ArgumentOutOfRangeException("msb", "velocity で指定できるのは 0 ～ 127 の範囲までです。");
			if (lsb > 127)
				throw new ArgumentOutOfRangeException("lsb", "velocity で指定できるのは 0 ～ 127 の範囲までです。");

			SendMessage(new[] { (byte)(0xB0 + channel), (byte)0x00, msb });
			SendMessage(new[] { (byte)(0xB0 + channel), (byte)0x20, lsb });
			SendMessage(new[] { (byte)(0xC0 + channel), pc });
		}

		#endregion

		#region Midi Clock

		public void ClockStart()
		{
			if (this.midiClock == null) return;

			this.midiClock.Start(false);

			Task.Factory.StartNew(() =>
				{
					while (this.midiClock.IsRunning)
					{
						if (this.Tick != null)
						{
							var tick = this.midiClock.ElapsedTick;
							var time = this.midiData.GetTime(tick);

							this.Tick(this,
									  new TickEventArgs(this.midiClock.ElapsedTime, tick,
							                            time.Measure, time.Beat + 1, time.Tick));
						}
					}
				});
		}

		public void ClockStop()
		{
			if (this.midiClock == null) return;
			this.midiClock.Stop();
		}

		public void ClockReset()
		{
			if (this.midiClock == null) return;
			this.midiClock.Reset();
		}

		#endregion

		#region Midi Data

		public MidiTrackCollection Tracks
		{
			get { return new MidiTrackCollection(this.midiData); }
		}

		#endregion

		public void Play()
		{
			this.IsPlaying = true;
			ClockStart();

		}

		public void Stop()
		{
			this.IsPlaying = false;
			ClockStop();
		}


		private int oldTick = -1;
		void MidiManager_Tick(object sender, TickEventArgs e)
		{
			if (!this.IsPlaying) return;

			lock (this)
			{
				var tick = e.ElapsedTick;

				if (tick == this.oldTick) return;

				foreach (var track in this.Tracks)
				{
					var events = track.MidiEvents.Where(w => oldTick <= w.StartTime && w.StartTime < tick);
					var mOut = this.MidiOut.Skip(track.OutputPort).FirstOrDefault();

					if (mOut == null)
						return;

					foreach (var ev in events)
					{
						Console.WriteLine("Event Start: {0}, Type: {1}, Channel: {2}", ev.StartTime, ev.EventType, ev.Channel);
						switch (ev.EventType)
						{
							case MidiEventType.NoteOff:
							case MidiEventType.NoteOn:
							case MidiEventType.ControlChange:
							case MidiEventType.ProgramChange:
							case MidiEventType.SysExStart:
							case MidiEventType.SysExContinue:
							case MidiEventType.PitchBend:
								mOut.SendMessage(ev.RawData);
								break;
							case MidiEventType.Tempo:
								this.Tempo = ev.Tempo;
								break;
							case MidiEventType.EndofTrack:
								break;
							default:
								mOut.SendMessage(ev.RawData);
								break;
						}
					}
				}

				this.oldTick = tick;
			}

			Thread.Sleep(0);
		}

		public void Close()
		{
			if (this.midiOut != null)
				foreach (var mo in this.midiOut)
					mo.Close();

			if (this.midiIn != null)
				foreach (var mi in this.midiIn)
					mi.Close();

			if (this.midiClock != null)
				this.midiClock.Close();

			if (this.midiStatus != null)
				this.midiStatus.Close();

			if (this.midiData != null)
				this.midiData.Close();
		}

		public void Dispose()
		{
			Close();
		}
	}
}
