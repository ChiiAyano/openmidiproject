﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenMidiProject.Data;

namespace OpenMidiProject.Midi
{
	public class MidiTrackCollection:ICollection<MidiTrack>
	{
		private MidiData midiData;

		internal MidiTrackCollection(MidiData midiData)
		{
			this.midiData = midiData;
		}

		public void Add(MidiTrack item)
		{
			this.midiData.AddTrack(item);
		}

		public void AddRange(IEnumerable<MidiTrack> items)
		{
			this.midiData.AddRangeTrack(items);
		}

		public void Clear()
		{
			this.midiData.RemoveAllTrack();
		}

		public bool Contains(MidiTrack item)
		{
			return this.midiData.Tracks.FirstOrDefault(f => f == item) != null;
		}

		public void CopyTo(MidiTrack[] array, int arrayIndex)
		{
			throw new NotImplementedException();
		}

		public int Count
		{
			get { return this.midiData.TrackCount; }
		}

		public bool IsReadOnly
		{
			get { throw new NotImplementedException(); }
		}

		public bool Remove(MidiTrack item)
		{
			this.midiData.RemoveTrack(item);
			return true;
		}

		public IEnumerator<MidiTrack> GetEnumerator()
		{
			return this.midiData.Tracks.GetEnumerator();
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return this.midiData.Tracks.GetEnumerator();
		}
	}
}
