﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMidiProject.Midi
{
	public class TickEventArgs : EventArgs
	{
		/// <summary>
		/// 現在の経過時間を取得します。
		/// </summary>
		public TimeSpan ElapsedTime { get; private set; }
		/// <summary>
		/// 現在の経過 Tick 数を取得します。
		/// </summary>
		public int ElapsedTick { get; private set; }

		/// <summary>
		/// 現在の小節数を取得します。
		/// </summary>
		public int Measure { get; private set; }
		/// <summary>
		/// 現在の拍数を取得します。
		/// </summary>
		public int Beat { get; private set; }
		/// <summary>
		/// 次の拍までの経過 Tick 数を取得します。
		/// </summary>
		public int Tick { get; private set; }

		public TickEventArgs(TimeSpan time, int totalTick, int measure, int beat, int tick)
		{
			this.ElapsedTime = time;
			this.ElapsedTick = totalTick;
			this.Measure = measure;
			this.Beat = beat;
			this.Tick = tick;
		}
	}
}
